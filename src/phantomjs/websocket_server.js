phantom.injectJs("core.js");
phantom.injectJs("autobahn.min.js");

// WAMP server
var wsuri = "ws://localhost:9000";

ab.connect(wsuri,

   // WAMP session was established
   function (session) {

      // asynchronous RPC, returns promise object
      session.call("http://example.com/simple/calc#add",
                   23, 7).then(

         // RPC success callback
         function (res) {
            console.log("got result: " + res);
         },

         // RPC error callback
         function (error, desc) {
            console.log("error: " + desc);
         }
      );
   },

   // WAMP session is gone
   function (code, reason) {
      console.log(reason);
   }
);