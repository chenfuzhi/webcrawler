phantom.injectJs("core.js");
phantom.injectJs("autobahn.min.js");

//WAMP session object
var sess;
var wsuri = "ws://192.168.2.207:3222";
 
 
// connect to WAMP server
ab.connect(wsuri,
 
      // WAMP session was established
      function (session) {
 
         sess = session;
         console.log("Connected to " + wsuri);
 
         // subscribe to topic, providing an event handler
         sess.subscribe("http://example.com/simple", function(topic, event){
        	 console.log("ddddddddddddddddddddddddddddd");
         });
     
         callProcedure();
      },
 
      // WAMP session is gone
      function (code, reason) {
 
         sess = null;
         console.log("Connection lost (" + reason + ")");
      }
);
 


function publishEvent()
{
   sess.publish("http://example.com/simple", {a: "foo", b: "bar", c: 23});
}

function callProcedure() {
   // issue an RPC, returns promise object
   sess.call("http://example.com/simple/calc#add", 23, 7).then(
 
      // RPC success callback
      function (res) {
         console.log("got result: " + res);
      },
 
      // RPC error callback
      function (error, desc) {
         console.log("error: " + desc);
      }
   );
}