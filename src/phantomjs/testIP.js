
console.log(phantom.args);
// Polyfills
if (typeof Function.prototype.bind !== "function") {
    Function.prototype.bind = function(scope) {
        "use strict";
        var _function = this;
        return function() {
            return _function.apply(scope, arguments);
        };
    };
}

function patchRequire(require, requireDirs) {
    "use strict";
    require('webserver'); // force generation of phantomjs' require.cache for the webserver module
    var fs = require('fs');
    var phantomBuiltins = ['fs', 'webpage', 'system', 'webserver'];
    var phantomRequire = phantom.__orig__require = require;
    var requireCache = {};
    function possiblePaths(path, requireDir) {
        var dir, paths = [];
        if (path[0] === '.') {
            paths.push.apply(paths, [
                fs.absolute(path),
                fs.absolute(fs.pathJoin(requireDir, path))
            ]);
        } else if (path[0] === '/') {
            paths.push(path);
        } else {
            dir = fs.absolute(requireDir);
            while (dir !== '' && dir.lastIndexOf(':') !== dir.length - 1) {
                paths.push(fs.pathJoin(dir, 'modules', path));
                // nodejs compatibility
                paths.push(fs.pathJoin(dir, 'node_modules', path));
                dir = fs.dirname(dir);
            }
            paths.push(fs.pathJoin(requireDir, 'lib', path));
            paths.push(fs.pathJoin(requireDir, 'modules', path));
        }
        return paths;
    }
    var patchedRequire = function _require(path) {
        var i, paths = [],
            fileGuesses = [],
            file,
            module = {
                exports: {}
            };
        if (phantomBuiltins.indexOf(path) !== -1) {
            return phantomRequire(path);
        }
        requireDirs.forEach(function(requireDir) {
            paths = paths.concat(possiblePaths(path, requireDir));
        });
        paths.forEach(function _forEach(testPath) {
            fileGuesses.push.apply(fileGuesses, [
                testPath,
                testPath + '.js',
                testPath + '.coffee',
                fs.pathJoin(testPath, 'index.js'),
                fs.pathJoin(testPath, 'index.coffee'),
                fs.pathJoin(testPath, 'lib', fs.basename(testPath) + '.js'),
                fs.pathJoin(testPath, 'lib', fs.basename(testPath) + '.coffee')
            ]);
        });
        file = null;
        for (i = 0; i < fileGuesses.length && !file; ++i) {
            if (fs.isFile(fileGuesses[i])) {
                file = fileGuesses[i];
            }
        }
        if (!file) {
            throw new Error("CasperJS couldn't find module " + path);
        }
        if (file in requireCache) {
            return requireCache[file].exports;
        }
        var scriptCode = (function getScriptCode(file) {
            var scriptCode = fs.read(file);
            if (/\.coffee$/i.test(file)) {
                /*global CoffeeScript*/
                scriptCode = CoffeeScript.compile(scriptCode);
            }
            return scriptCode;
        })(file);
        var fn = new Function('__file__', 'require', 'module', 'exports', scriptCode);
        try {
            fn(file, _require, module, module.exports);
        } catch (e) {
            var error = new window.CasperError('__mod_error(' + path + '):: ' + e);
            error.file = file;
            throw error;
        }
        requireCache[file] = module;
        return module.exports;
    };
    patchedRequire.patched = true;
    return patchedRequire;
}

var fs = (function _fs(fs) {
    if (!fs.hasOwnProperty('basename')) {
        fs.basename = function basename(path) {
            return path.replace(/.*\//, '');
        };
    }
    if (!fs.hasOwnProperty('dirname')) {
        fs.dirname = function dirname(path) {
            return path.replace(/\\/g, '/').replace(/\/[^\/]*$/, '');
        };
    }
    if (!fs.hasOwnProperty('isWindows')) {
        fs.isWindows = function isWindows() {
            var testPath = arguments[0] || this.workingDirectory;
            return (/^[a-z]{1,2}:/i).test(testPath) || testPath.indexOf("\\\\") === 0;
        };
    }
    if (!fs.hasOwnProperty('pathJoin')) {
        fs.pathJoin = function pathJoin() {
            return Array.prototype.join.call(arguments, this.separator);
        };
    }
    return fs;
})(require('fs'));

phantom.casperPath = "/home/cfz/test/n1k0-casperjs-8c798c7";
window.CasperError = function CasperError(msg) {
    Error.call(this);
    this.message = msg;
    this.name = 'CasperError';
};
window.CasperError.prototype = Object.getPrototypeOf(new Error());

window.require = patchRequire(window.require, [phantom.casperPath, fs.workingDirectory]);

var clientUtilsPath = require('fs').pathJoin(phantom.casperPath, 'modules', 'clientutils.js');
//phantom.casperArgs.drop(phantom.casperScript);
//phantom.injectJs("alexa.js");
var casper = require('casper').create({
	verbose: true, 
	logLevel: "debug",
    pageSettings: {
    	//userAgent:"192.168.2.202:3128",
        loadImages:  false,        // The WebPage instance used by Casper will
        loadPlugins: false         // use these settings
    }
  });

casper.start("http://www.baidu.com/s?wd=ip", function(){
this.debugHTML("p.op_ip_detail");
});

casper.run();
