var casper = require('casper').create({
										verbose: true, 
										logLevel: "debug",
									    pageSettings: {
									    	//userAgent:"http://192.168.2.202:3128",
									        //loadImages:  false,        // The WebPage instance used by Casper will
									        //loadPlugins: false         // use these settings
									    }
									  });


var urls = {
				index: geurl('http://www.wealink.com/'),
				jobs: geurl('http://www.wealink.com/jobs/'),
				company: geurl('http://www.wealink.com/company/'),
				renmai: geurl('http://www.wealink.com/renmai/')
			};

function geurl(url){
	if(url.indexOf("?") == -1)
		return url + "?utm_source=wlan&utm_medium=robot&utm_campaign=candy";
	else
		return url + "&utm_source=wlan&utm_medium=robot&utm_campaign=candy";
};


var openCount = 100;
var maxCount = 100;


exec_next = function(_casper, css3, nextPage){
	var rand = parseInt(Math.random()*10) + 1;
	var css3 = "div.container2 li:nth-child($rand) div.desc a";
	css3 = css3.replace("\$rand", rand);
    if (_casper.exists(css3)) {
    	_casper.click(css3);
    } else {
    	_casper.echo(css3+"not find", 'ERROR');
    }
	openCount--;
	if(openCount > 0){
		
		_casper.open(geurl(nextPage + rand*10)).then(fun_jobs);
	}else{
		openCount = maxCount;
	}
}

fun_jobs = function() {
	exec_next(this,
			  "div.container2 li:nth-child($rand) div.desc a",
			  "http://www.wealink.com/jobs/?offset="
			 );
};

fun_company = function(){
	exec_next(this,
			  "div.container2 li:nth-child($rand) div.desc a",
			  "http://www.wealink.com/company/?offset="
			 );
};

fun_renmai = function(){
};


casper.start(urls.index);
casper.thenOpen(urls.jobs, fun_jobs);
casper.thenOpen(urls.company, fun_company);
casper.thenOpen(urls.renmai, fun_renmai);


casper.run(function() {
    this.echo("finish").exit();
});
