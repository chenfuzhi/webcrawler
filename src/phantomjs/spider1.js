phantom.injectJs("core.js");
phantom.injectJs("autobahn.min.js");


window.onload = function() {
	 
	   // connect to WAMP server
	   ab.connect("ws://localhost:9000",
	 
	      // WAMP session was established
	      function (session) {
	 
	         // things to do once the session has been established
	 
	      },
	 
	      // WAMP session is gone
	      function (code, reason) {
	 
	         // things to do once the session fails
	      }
	   );
};
