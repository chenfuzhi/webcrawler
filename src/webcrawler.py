#coding:utf-8


#run_repeater
#if __name__ == '__main__':
#    import sys
#    import os
##    reset = raw_input("reset localhost, 27017 database: y/n:")
##    if reset == "y":
##        test_init_data.initData("localhost", 27017)
##    
#    log.startLogging(sys.stdout)
#    factory = StoreServerFactory("ws://localhost:9000", debugWamp = True)
#    factory.protocol = RepeaterServerProtocol
#    factory.setProtocolOptions(allowHixie76 = True)
#    listenWS(factory)
#    reactor.run()
import sys
import os
import sys, math

from twisted.python import log
from twisted.internet import reactor, defer
from twisted.web.server import Site
from twisted.web.static import File

from autobahn.websocket import listenWS
from autobahn.websocket import connectWS
from autobahn.wamp import exportSub, \
                          exportPub, \
                          exportRpc, \
                          WampServerFactory, \
                          WampServerProtocol

import json
from bson.objectid import ObjectId
from  subprocess import Popen
from multiprocessing import Process
from repeater import StoreServerFactory, RepeaterServerProtocol
from spider2 import SpiderClientFactory, TaskClientProtocol



import socket

def get_unuse_port(host="localhost", start=10000, end=20000):
    for i in xrange(start,end):
        result = None
        TIME_OUT = 2
        sk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sk.settimeout(TIME_OUT)
        try:
            sk.bind((host, i))
            sk.close()
            return i
        except socket.error:
            continue
        

def run_repeater(port):
    log.startLogging(sys.stdout)
    factory = StoreServerFactory("ws://localhost:%s" % port, debugWamp = True)
    factory.protocol = RepeaterServerProtocol
    factory.setProtocolOptions(allowHixie76 = True)
    listenWS(factory)


    factory = SpiderClientFactory("ws://localhost:%s" % port)
    factory.protocol = TaskClientProtocol
    connectWS(factory)
   
    reactor.run()



def run_ui(host="0.0.0.0", port=5000):
    from api import app
    app.run(host=host, port=port, debug=True)


__RUNNING__ = []
__repeater_port__ = None
import time
if __name__ == "__main__":
    

#    ui = Process(target=run_ui, kwargs={"host":"0.0.0.0", "port":5000})
#    ui.start()
#    
    #log.msg("程序准备启动。。。")
    repeater = Process(target=run_repeater, args=("10000",))
    repeater.start()
    
    
    

    
    
    
    
    
    
