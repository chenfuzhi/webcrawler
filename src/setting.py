#-*- coding:utf-8 -*-
BOT_NAME = 'dcrawler'
BOT_VERSION = '1.0'

SPIDER_MODULES = ['project.spiders']
NEWSPIDER_MODULE = 'project.spiders'

# redis linkdb
REDIS_HOST = 'localhost'
REDIS_PORT = 6379

# mongodb config
SYSMONGO_HOST = 'localhost'
SYSMONGO_PORT = 27017
SYSMONGO_DB = 'sys'

# item store mongodb host
STORE_HOST = "localhost"
STORE_PORT = 27017

# link store mongodb host
LINK_HOST = "localhost"
LINK_PORT = 27017





