Ext.define('runnner.model.runner', {
    extend: 'Ext.data.Model',
    fields: ['name', 'cuisine', 'description', {name:'rating', type:'float'}]
});
