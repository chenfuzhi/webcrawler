Ext.define('runner.view.Viewport', {
    extend: 'Ext.container.Viewport',
    requires: [
        'Ext.layout.container.Border',
        'Ext.layout.container.HBox'
    ],
    
    layout: 'border',
    
    items: [
        {
            region: 'center',
            xtype : 'panel',
            id: 'console',
            autoScroll: true,
            //html: '<object type="application/x-qt-plugin" width="100%", height="100%" classid="Console" />',
            width: '100%'
        },
        {
            region: 'east',
            xtype: 'panel',
            title: '控制台',
            width: 200,
            minWidth: 175,
            collapsible: true,
            split: true,
            layout:{
            	type:'border'
            },
            items:[
                Ext.create('Ext.toolbar.Toolbar', {
				    renderTo: document.body,
				    region: 'north',
				    width   : "100%",
				    height : 30,
				    items: [
				        {
				            // xtype: 'button', // default for Toolbars
				            text: '全部显示',
				            width   : "100%",
				            handler: function(){
				            	var html = '<object type="application/x-qt-plugin" width="100%", height="100%" classid="Console" />';
				            	var panel = Ext.getCmp("console");
				            	//panel.getEl().setHTML(html);
				            	console.log(panel);
				            	//panel.html = 'sssssssssssss';
				            }
				        }
				    ]
				}),
				{
					xtype: 'RunnerList',
					text: '列表',
					height: "100%",
					region: 'center'
				},
				Ext.create('Ext.toolbar.Toolbar', {
				    renderTo: document.body,
		        	region: 'south',
				    height : 30,
				    items: [
				        {
				            // xtype: 'button', // default for Toolbars
				            text: '退出',
				            width   : "100%",
				            handler: function(){
				            	Ext.Msg.show({
							     title:'警告',
							     msg: '是否确定删除？',
							     buttons: Ext.Msg.YESNO,
							     icon: Ext.Msg.QUESTION,
							     fn: function(btn){
							     	if(btn == 'yes'){
							     		console.log('click yes');
							     	}
							     }
								});
				            }
				        }
				    ]
				})
            ]
        }
    ]
});
