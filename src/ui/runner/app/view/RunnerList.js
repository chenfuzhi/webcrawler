Ext.define('runner.view.RunnerList', {
    extend: 'Ext.grid.Panel',
    xtype: 'RunnerList',
    store: 'Runner',
    hideHeaders : true,
    selModel: {
                mode: 'SINGLE',
                listeners: {
                    scope: this,
                    selectionchange: function(){
                    	console.log("selectionchange");
                    }
                }
    },
	columns: [
        { text: 'Name',  dataIndex: 'name', width:'100%'}
    ]
});