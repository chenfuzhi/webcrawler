Ext.define('runner.controller.Main', {
    extend: 'Ext.app.Controller',
    stores: [
    	'Runner'
    ],
    views: [
        'Viewport',
        'RunnerList'
    ],

    refs: [    
    ],

	requires: [
    ],
    init: function() {
    	
    }

});
