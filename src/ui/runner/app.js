Ext.application({
    name: 'runner',

    autoCreateViewport: true,
    
    requires: [
        'Ext.window.MessageBox'
    ],

    controllers: [
        'Main'
    ],
    ui: 'gray',
    launch: function(){
    	//Ext.MessageBox.alert("1111111", "ddd");
    	
		var sess;
		var wsuri = "ws://localhost:9999";
		 
				// 创建一个Socket实例
		var socket = new WebSocket('ws://localhost:9999'); 
		
		// 打开Socket 
		  socket.onopen = function(event) { 
		
		  // 发送一个初始化消息
		  socket.send('I am the client and I\'m listening!'); 
		
		  // 监听消息
		  socket.onmessage = function(event) { 
		    console.log('Client received a message',event); 
		    Ext.getCmp("console").getEl().insertHtml("afterBegin", event.data + "<br/>");
		  }; 
		
		  // 监听Socket的关闭
		  socket.onclose = function(event) { 
		    console.log('Client notified socket has closed',event); 
		  }; 
		
		  // 关闭Socket.... 
		  //socket.close() 
		};

        if (!Ext.isWebKit) {
            Ext.MessageBox.alert('WebKit Only', 'This example is currently only supported in WebKit browsers');
        }
    }
});
