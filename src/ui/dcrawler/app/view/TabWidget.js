Ext.define('dcrawler.view.TabWidget', {
    extend: 'Ext.tab.Panel',
    requires: [
    	'dcrawler.view.Welcome'
    ],
    xtype: 'main_tab',
    layout: 'border',
    items: [
        {
            region: 'north',
            xtype : 'welcome',
            bodyPadding: 5,
            title: '欢迎页面',
            closable: false
        }
    ]
});
