Ext.define('dcrawler.view.LeftTree', {
    extend: 'Ext.tree.Panel',
    xtype: 'left_tree',

    requires: [
       'dcrawler.store.Task',
       'dcrawler.view.widgets.TaskTab'
    ],
    rootVisible: false,
	
	//cls: 'examples-list',
    
    lines: false,
    useArrows: true,
    enableColumnHide: false,
    enableColumnResize: false,
    columns:[
    		{
    			xtype:'treecolumn',
    			text: '管理树',
    			dataIndex:'text',
    			flex:1,
    			sortable:false
    			
    		}
    ],
    store: {
        autoload: true,
        fields: ['id', 'alias', 'text'],
        root: {
            expanded: true,
            text: "root",
            id: 'webcrawler',
            root: true,
            children: [
                       { 
                    	   text: "运行蜘蛛",
                    	   id: "spider"
                    	   
                       },
                       { 
                    	   id: "tasker",
                    	   text: "任务管理", 
                       }
                   ]
        }
    },
    listeners: {
    	beforeitemexpand: function(item){
    		
		},
		itemdblclick: function( tree, record, item, index, e, eOpts ){
			if(record.data.id == "spider"){
				refreshSpider();
    		}else if(record.data.id == "tasker"){
    			refreshTasker();
    		}else if(record.parentNode.data.id == "tasker"){
    			var tab = Ext.getCmp("id_main_tab");
    			
    			var task_panel = Ext.getCmp(record.data.id);
    			if(task_panel == undefined){
    				console.log("not load");
    			   task_panel = Ext.create("dcrawler.view.widgets.TaskTab",{
	    				title: record.data.text,
	    				id: record.data.id
    				});
    				tab.add(task_panel);
    			}else{
    				task_panel.reload();
    			}
    			tab.setActiveTab(task_panel);
    		}
		}
    },
    
    initComponent: function() {
        this.callParent();
        
        ab_session.subscribe("admin", refreshSpider);
        this.task_store = Ext.create("dcrawler.store.Task");
        setTimeout(function(){
        	refreshTasker();
	        refreshSpider();
        }, 3);
    }
});

function refreshTasker(){
	var left_tree = Ext.getCmp('left_tree');
	var store = left_tree.task_store;
	var node_tasker = left_tree.getStore().getNodeById('tasker');
	store.load({
		params:{"node":"tasker"},
		callback : function(datas, op, success){
			node_tasker.removeAll(true);
			node_tasker.collapse();
			Ext.Array.each(datas, function(name, index, countriesItSelf) {
				console.log(name.data);
				node_tasker.appendChild(name.data);
			});
			node_tasker.expand();
		}
	});
}
function refreshSpider(){
	ab_session.call("clients", "spider").then(function(res){
		console.log(res);
		var store = Ext.getCmp('left_tree').getStore();
		
		node = store.getNodeById('spider');
		node.removeAll(true);
		node.collapse();
		Ext.Array.each(res, function(name){
			node.appendChild({
				'text': name.peer,
				'id': name.id,
				"alias": name.peer,
				"leaf": 1
			});
		});
		node.expand();
	});
}
