Ext.define('dcrawler.view.menus.tree', {
    extend: 'Ext.menu.Menu',
    xtype : 'menu_tree',
    items: [{
    	id: 'del_server',
        text: '增加服务器'
    }],
    $switchNode: function(node){
    	this.removeAll();
    	if(node.parentNode.raw.id == "root"){
    		if(node.raw.id == "spider"){
    			this.add({'text': '添加蜘蛛', 'id':'add_spider'})
	    	}else if(node.raw.id == "runner"){
	    		this.add({'text': '添加服务器', 'id':'add_runner'})
	    	}
    	}else{
    		if(node.parentNode.raw.id == "spider"){
    			this.add({'text': '删除蜘蛛', 'id':'delete_spider'})
	    	}else if(node.parentNode.raw.id == "runner"){
	    		this.add({'text': '删除服务器', 'id':'delete_runner'})
	    	}
    	}
    	console.log(node);
    },
    listeners: {
        click: {
            fn: function(menu, item, e, eOpts){
            	
            	switch(item.id){
            	case "delete_spider":
            		Ext.Msg.show({
				     title:'警告',
				     msg: '是否确定删除？',
				     buttons: Ext.Msg.YESNO,
				     icon: Ext.Msg.QUESTION,
				     fn: function(btn){
				     	if(btn == 'yes'){
				     		var node = Ext.getCmp('left_tree').getSelectionModel().getSelection()[0];
							node.remove();
							Ext.getCmp('left_tree').getStore().sync();
				     	}
				     }
					});
            		break;
            	case "delete_runner":
            		Ext.Msg.show({
				     title:'警告',
				     msg: '是否确定删除？',
				     buttons: Ext.Msg.YESNO,
				     icon: Ext.Msg.QUESTION,
				     fn: function(btn){
				     	if(btn == 'yes'){
				     		var node = Ext.getCmp('left_tree').getSelectionModel().getSelection()[0];
							node.remove();
							Ext.getCmp('left_tree').getStore().sync();
				     	}
				     }
					});
            		break;
            	case "add_spider":
            	 	var tree = Ext.getCmp('left_tree');
            		var spider = tree.getStore().getNodeById("spider");
            		var newspider = spider.appendChild({
            			text: 'undifind',
            			leaf: true
            		});
            		var plg = tree.getPlugin("spider_edit");
            		//console.log(tree.columns);
            		plg.startEdit(newspider, tree.columns[0]);
            		var edit = tree.columns[0].getEditor();
					edit.on('focus', function(edit, opt){
						edit.selectText();
					});
            		break;
            	case "add_runner":
            		var tree = Ext.getCmp('left_tree');
            		var spider = tree.getStore().getNodeById("runner");
            		var newspider = spider.appendChild({
            			text: 'undifind',
            			leaf: true
            		});
            		var plg = tree.getPlugin("spider_edit");
            		plg.startEdit(newspider, tree.columns[0]);
            		
            		break;
            	case "":
            		break;
            	}
            }
        }
    },
    initComponent: function() {
        this.callParent();
    }
});