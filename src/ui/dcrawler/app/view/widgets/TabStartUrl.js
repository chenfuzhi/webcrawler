Ext.define('dcrawler.view.widgets.TabStartUrl', {
    extend: 'Ext.panel.Panel',
    xtype: 'TabStartUrl',
    requires: [
    	'dcrawler.model.StartUrl'
    ],
    title: '起始地址',
	items:[
		{
			xtype: 'gridpanel',
			columns: [
				{ text: '序号', width:50},
		        { text: '内容', width:400},
		        { text: '类型' }
		    ],
		    enableColumnHide: false,
		    enableColumnMove: false,
		    sortableColumns : false,
		    enableColumnResize : false
		}
	],
	reload: function(task){
		this.task = task;
	},
	dockedItems: [{
	    xtype: 'toolbar',
	    dock: 'bottom',
	    ui: 'footer',
	    defaults: {minWidth: this.minButtonWidth},
	    items: [
	        { 
	        	xtype: 'button', 
	        	text: '添加',
	        	handler: function(){
	        		dlg = Ext.create('dcrawler.view.dlg.StartUrl',{
	        			task_id: this.up('TabStartUrl').task.data.id,
	        			buttons:[
	        				{
	        					xtype: 'button',
	        					text: '确定',
	        					handler: function(){
	        						
	        						var form = this.up('StartUrl').query('form')[0].getForm();
	        						var record = form.getRecord();
	        						form.updateRecord(record);
	        						record.save({ 
				                        success: function(user) {
				                        	console.log(user);
				                            Ext.Msg.alert('Success', 'User saved successfully.')
				                        },
				                        failure: function(user) {
				                            Ext.Msg.alert('Failure', 'Failed to save user.')
				                        }
				                    });
	        					}
	        				},
	        				{
	        					xtype: 'button',
	        					text: '取消',
	        					handler: function(){
	        						console.log(this);
	        					}
	        				}
						]
	        		}).show(null, function(){
	        			
	        			var obj_id = ObjectId().toString();
	        			var def = Ext.create('dcrawler.model.StartUrl',{
	        				'id': null,
	        				'url_id': obj_id,
	        				'url_type': 'increase',
	        				'url': '',
	        				'linkdb_id': '',
	        				'task_id': this.task_id
	        			});
						this.query('form')[0].loadRecord(def);	        			
	        		});
	        	}
	         }
	    ]
	}],
	listeners:{
    	afterrender: function(panel, op){
    		console.log("TaskStartUrl");
    	}
    },
});