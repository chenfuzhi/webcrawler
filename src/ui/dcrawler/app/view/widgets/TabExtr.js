Ext.define('dcrawler.view.widgets.TabExtr', {
    extend: 'Ext.panel.Panel',
    xtype: 'TabExtr',
    requires: [
    ],
    title: '提取规则',
    reload: function(task){
		
	},
	items:[
		{
			xtype: 'gridpanel',
			columns: [
		        { text: '内容', width:400},
		        { text: '类型' }
		    ],
		    sortableColumns : false
		}
	]
});