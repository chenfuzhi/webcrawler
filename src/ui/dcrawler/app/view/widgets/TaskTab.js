Ext.define('dcrawler.view.widgets.TaskTab', {
    extend: 'Ext.panel.Panel',
    requires: [
    	'dcrawler.model.TabInfo',
    	'dcrawler.view.widgets.TabStartUrl',
    	'dcrawler.view.widgets.TabNavi',
    	'dcrawler.view.widgets.TabExtr',
    	'dcrawler.view.widgets.TabLinkdb',
    	'dcrawler.view.widgets.TabInfo',
    ],
    layout: 'vbox',
    items:[
    
    		{
		        xtype: 'form',
		        width: '100%',
		        height: 600,
		        bodyPadding: 10,
		        items:[
				        {
					        // Fieldset in Column 1 - collapsible via toggle button
					        xtype:'fieldset',
					        title: '状态',
					        items :[
									{
		    							xtype: 'displayfield',
		    							name: 'id',
		    							fieldLabel: 'id',
		    							labelWidth: 80,
		    						},
		    						{
		    							xtype: 'displayfield',
		    							name: 'status',
		    							fieldLabel: '状态',
		    							labelWidth: 80,
		    						}
					        ]
					    },
					    {
					        xtype:'fieldset',
					        title: '导航信息',
					        layout: 'vbox',
					        items :[
					        ]
					    },
					    {
			    			xtype:'tabpanel',
			    			width: '100%',
			    			height: 200,
			    			bodyPadding: 5,
			    			items:[
			    				{
			    					xtype: 'TabInfo',
			    				},
			    				{
			    					xtype: 'TabStartUrl',
			    				},
			    				{
			    					xtype: 'TabLinkdb',
			    				},
			    				{
			    					xtype: 'TabExtr',
			    				}
			    			]
			    		}
		        ]
		        
		    }
    		
    ],
    reload: function(){
    	this.setLoading("loading..");
        var task_info = dcrawler.model.TabInfo;
        task_info.load(this.id,{
        	success:function(record){
        		console.log(record);
        		var tab_task = Ext.getCmp(record.data.id);
        		
        		var form = tab_task.query("form")[0]
        		form.loadRecord(record);
        		//tab_task.reloadStartUrl();
        		tab_task.query('TabStartUrl')[0].reload(record);
        		tab_task.setLoading(false);
        	}
        });
    },
    listeners:{
    	afterrender: function(panel, op){
    		console.log("TaskTab");
    		this.reload();
    	}
    },
    constructor: function(config) {
        config.closable = true;
        this.callParent([config]);
    },
    initComponent: function() {
        this.callParent();
    }
});
