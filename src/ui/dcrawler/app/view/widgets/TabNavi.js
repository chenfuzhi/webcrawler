Ext.define('dcrawler.view.widgets.TabNavi', {
    extend: 'Ext.panel.Panel',
    xtype: 'TabNavi',
    requires: [
    ],
    title: '导航规则',
    reload: function(task){
		
	},
	items:[
		{
			xtype: 'gridpanel',
			columns: [
		        { text: '内容', width:400},
		        { text: '类型' }
		    ],
		    sortableColumns : false
		}
	]
});