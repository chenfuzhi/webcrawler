Ext.define('dcrawler.view.widgets.TabInfo', {
    extend: 'Ext.panel.Panel',
    xtype: 'TabInfo',
    requires: [
    ],
    title: '任务信息',
    reload: function(task){
		this.query('form')[0].loadRecord(task);
	},
	items:[
			{
		        xtype:'form',
		        layout: 'vbox',
		        border: 0,
		        items :[
						{
	    					xtype: 'textfield',
	    					fieldLabel: '任务名',
	    					allowBlank: false,
	    					name: 'alias',
	    					labelWidth: 80,
	    					width: 300
	    				},
	    				{
	    					xtype: 'numberfield',
	    					fieldLabel: '运行次数',
	    					allowBlank: false,
	    					name: 'run_times',
	    					maxValue: 86400,
							minValue: 0,
	    					labelWidth: 80,
	    				},
						{
	    					xtype: 'numberfield',
	    					fieldLabel: '延迟时间',
	    					allowBlank: false,
	    					name: 'delay',
	    					maxValue: 86400,
							minValue: 0,
	    					labelWidth: 80,
	    				}
		        ]
		    }
	],
	buttons:[
    	{ text: '保存' }
	]
});