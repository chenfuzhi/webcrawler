Ext.define('dcrawler.view.widgets.TabLinkdb', {
    extend: 'Ext.panel.Panel',
    xtype: 'TabLinkdb',
    requires: [
    ],
    title: '地址类型',
    reload: function(task){
		
	},
	items:[
		{
			xtype: 'gridpanel',
			columns: [
		        { text: '内容', width:400},
		        { text: '类型' }
		    ],
		    sortableColumns : false
		}
	]
});