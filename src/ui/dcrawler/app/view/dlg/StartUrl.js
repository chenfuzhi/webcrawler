Ext.define('dcrawler.view.dlg.StartUrl', {
    extend: 'Ext.window.Window',
    requires: [
    	'dcrawler.store.Linkdbs'
    ],
    xtype:'StartUrl',
    modal:true,
    title: '起始地址',
    items: [
        {
            xtype: 'form',
            //frame:true,
            bodyPadding: 13,
            height: null,
            defaults: { anchor: '100%' },
            fields: [
					{name: 'url_id', type: 'string'},
					{name: 'url_type', type: 'string'},
					{name: 'url'},
					{name: 'linkdb_id', type: 'string'},
					],
            items: [
                { 
                	xtype: 'textfield',
                	allowBlank:false, 
                	fieldLabel: '地址', 
                	width: 500,
                	name: 'url',
                	emptyText: 'http://www.XXXX'
                },
                {
                	xtype: 'combobox',
                	fieldLabel: '类型',
				    store: Ext.create('Ext.data.Store', {
					    fields: ['name', 'url_type'],
					    data : [
					    	{'name': '静态', 'url_type': 'static'},
					    	{'name': '增长', 'url_type': 'increase'}
					    ]
					}),
					name: 'url_type',
					value: 'static',
					selectOnFocus : true,
					forceSelection: true,
				    queryMode: 'local',
				    displayField: 'name',
				    valueField: 'url_type',
				    allowBlank:false
                },
                {
                	xtype: 'combobox',
                	fieldLabel: '地址库',
				    store: Ext.create('dcrawler.store.Linkdbs'),
					value: 'static',
					name: 'linkdb_id',
					queryMode: 'remote',
					selectOnFocus : true,
					forceSelection: true,
				    displayField: 'alias',
				    valueField: 'id',
				    allowBlank:false, 
				    selectOnFocus:true,
				    listeners:{
				    	beforequery: function(com, op){
							this.store.task_id = this.up('StartUrl').task_id;
				    	}
				    }
                }
            ]

        }
    ]
});
