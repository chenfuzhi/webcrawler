Ext.define('dcrawler.view.Welcome', {
    extend: 'Ext.panel.Panel',
    xtype : 'welcome',
    
    ui   : 'sencha',
    height: 53,
    
    items: [
        {
            xtype: 'component',
            cls  : 'x-logo',
            html : '蜘蛛管理系统'
        }
    ]
});
