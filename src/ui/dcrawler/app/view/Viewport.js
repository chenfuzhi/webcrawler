Ext.define('dcrawler.view.Viewport', {
    extend: 'Ext.container.Viewport',
    requires: [
        'Ext.layout.container.Border',
        'Ext.layout.container.HBox',
        'dcrawler.view.LeftTree',
        'dcrawler.view.Header',
        'dcrawler.view.TabWidget',
    ],
    
    layout: 'border',
    
    items: [
        {
            region: 'north',
            xtype : 'pageHeader',
            bodyPadding: 5,
        },
        {
            region: 'center',
            
            layout: {
                type : 'hbox',
                align: 'stretch'
            },
            items: [
                {
                	id:'left_tree',
                    xtype: 'left_tree',
                    width: 300,
                    bodyPadding: 0
                },
                {
                    flex: 1,
                    id   : 'id_main_tab',
                    xtype: 'main_tab',
                    overflowY: 'auto',
                    bodyPadding: 0
                }
            ]
        }
    ]
});
