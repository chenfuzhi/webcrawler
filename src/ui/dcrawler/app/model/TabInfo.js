Ext.define('dcrawler.model.TabInfo', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'string'},
        {name: 'alias', type: 'string'},
        {name: 'delay'},
        {name: 'run_times', type: 'int'},
        {name: 'extrs'},
        {name: 'navi'},
        {name: 'prio'},
        {name: 'start_links'},
        {name: 'status'}
    ],
    proxy: {
        type: 'ajax',
        api: {
            read: '/api/task/info/'
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});