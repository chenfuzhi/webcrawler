Ext.define('dcrawler.model.StartUrl', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'url_id', type: 'string'},
        {name: 'url_type', type: 'string'},
        {name: 'url'},
        {name: 'linkdb_id', type: 'string'},
    ]
});