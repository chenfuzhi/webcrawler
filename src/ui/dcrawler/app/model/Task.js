Ext.define('dcrawler.model.Task', {
    extend: 'Ext.data.Model',
    fields: ['id', 'text', 'alias'],
    proxy: {
        type: 'ajax',
        api: {
        	read:'/api/task/read/'
        },
        reader: {
            type: 'json',
            root: 'data',
        }
    }
});
