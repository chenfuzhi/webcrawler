Ext.define('dcrawler.controller.Main', {
    extend: 'Ext.app.Controller',
    stores: [
    	'Task'
    ],

	
    views: [
        'Viewport',
        'Header'
    ],

    refs: [
    	{
            ref: 'lefttree',
            selector: 'LeftTree'
        }
    ],

	requires: [
        'dcrawler.view.menus.tree'
    ],
    init: function() {
    	
    	//store = this.getStore("Task");
//    	store = Ext.create("dcrawler.store.Task", {});
//    	store.load();
//    	console.log(store);
        
		this.menu_tree = Ext.create('dcrawler.view.menus.tree');
        this.control({
        	'viewport left_tree': {
            	"itemcontextmenu":function(tree, node, item, index, e, eOpts){
            		this.menu_tree.$switchNode(node);
            		this.menu_tree.showAt(e.getXY(), true);
			    	e.stopEvent();
			    }
			 }
        });
    }

});
