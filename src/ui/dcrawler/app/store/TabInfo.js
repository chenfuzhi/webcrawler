Ext.define('dcrawler.store.TabInfo', {
    extend: 'Ext.data.Store',
    fields: ['id', 'text', 'alias', 'leaf'],
    
    proxy: {
        type: 'ajax',
        api: {
        	read:'/api/task/info/'
        },
        reader: {
            type: 'json',
            root: 'data',
        }
    }
});