Ext.define('dcrawler.store.StartUrls', {
    extend: 'Ext.data.Store',
    fields: ['id', 'alias', 'encoding', 'duplicate'],
    proxy: {
        type: 'ajax',
        api: {
        	read:'/api/task/linkdbs/read/'
        },
        reader: {
            type: 'json',
            root: 'data',
        }
    },
    listeners: {
	    beforeload: function(store, options) {
	        options.params.id = store.task_id;
	   	}
	}
});