Ext.define('dcrawler.store.StartUrls', {
    extend: 'Ext.data.Store',
    model: 'dcrawler.model.StartUrl',
    proxy: {
        type: 'ajax',
        api: {
            read	: '/api/task/start_links/read/',
		    create  : '/api/task/start_links/create/',
		    update  : '/api/task/start_links/update/',
		    destroy : '/api/task/start_links/destroy/',
        },
        writer: {
        	type: 'json'
        },
        reader: {
            type: 'json',
            root: 'data'
        },
        //extraParams: {'task_id':"50ed210bac4be07e3100001a"}
    },
});