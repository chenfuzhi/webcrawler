Ext.define('dcrawler.store.Task', {
    extend: 'Ext.data.Store',
    fields: ['id', 'text', 'alias', 'leaf'],
    
    proxy: {
        type: 'ajax',
        api: {
        	read:'/api/task/read/'
        },
        reader: {
            type: 'json',
            root: 'data',
        }
    }
});