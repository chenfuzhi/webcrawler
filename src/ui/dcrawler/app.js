Ext.application({
    name: 'dcrawler',

    autoCreateViewport: true,
    
    requires: [
        'Ext.window.MessageBox'
    ],

    controllers: [
        'Main'
    ],
    ui: 'gray',
    launch: function(){
    	//Ext.MessageBox.alert("1111111", "ddd");
        if (!Ext.isWebKit) {
            Ext.MessageBox.alert('WebKit Only', 'This example is currently only supported in WebKit browsers');
        }
    }
});