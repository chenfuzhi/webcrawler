StartTest(function(t) {
    	// {name: 'id', type: 'string'},
        // {name: 'url_id', type: 'string'},
        // {name: 'url_type', type: 'string'},
        // {name: 'url'},
        // {name: 'linkdb_id', type: 'string'},
        // {name: 'task_id', type: 'string'},
    t.chain(
    	function(next){
    		t.requireOk([
    			'dcrawler.model.StartUrl',
    			'dcrawler.store.StartUrls'
    		], next);
    	},
    	function(next){
    		var store = Ext.create('dcrawler.store.StartUrls');
    		
    		store.getProxy().extraParams = {'task_id':"50ed210bac4be07e3100001a"}
    		var obj_id = ObjectId().toString();
    		store.add({
	    		url_id: obj_id,
	    		url: 'http://www.baidu.com/chenfuzhi',
	    		url_type: 'static',
	    		linkdb_id: '50ed210bac4be07e31000000',
	    		task_id : "50ed210bac4be07e3100001a"
	    	});
	    	store.sync({
	    		success : next
	    	})
    	},
    	function (next, batch, op) {
    		t.is(1, 1, 'add ok');
    		
    		console.log(batch);
    		var store = Ext.create('dcrawler.store.StartUrls', {
    			task_id : "50ed210bac4be07e3100001a"
    		});
			store.load({
				params: {
		        	start: 1,
		        	limit: 1
			    },
			    callback: next,
			    scope: this
			});
			
			store.insert(0, {
				'': ''
			});        
	    },
	    function (next, records, op, success) {
	    	
	    	t.is(success, true, 'load start=1, limit=1 ok')
	    	op.scope.insert(0, {
	    		
	    	}); 
	    	//t.is(op.success, true, 'create url success');
	    	
	    	//dcrawler.model.StartUrl.load(records[0].url_id, );
	    }
    );
});
