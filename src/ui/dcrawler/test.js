var Harness = Siesta.Harness.Browser.ExtJS;

Harness.configure({
    title       : 'dcrawler test',
    loaderPath  : { 'dcrawler' : 'app' },
    
    preload : [
        "../extjs/resources/css/ext-all.css",
        "../extjs/ext-all-debug.js",
        "Objectid.js",
    ]
    
});
 
Harness.start(
    {
        group               : 'Model',
        items               : [
            'tests/test_model.js'
        ]
    }
);

