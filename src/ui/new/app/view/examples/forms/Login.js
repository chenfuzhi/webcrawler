Ext.define('KitchenSink.view.examples.forms.Login', {
    extend: 'KitchenSink.view.examples.Example',
    requires: [
        'Ext.form.Panel',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Text'
    ],
    xtype: 'a123',
    alias: 'widget.chenfuzhi',
    items: [
        {
            xtype: 'form',
            
            title: 'Login',
            frame:true,
            bodyPadding: 13,
            height: null,
            
            defaultType: 'textfield',
            defaults: { anchor: '100%' },
            
            items: [
                { allowBlank:false, fieldLabel: 'User ID', name: 'user', emptyText: 'user id' },
                { allowBlank:false, fieldLabel: 'Password', name: 'pass', emptyText: 'password', inputType: 'password' },
                { xtype:'checkbox', fieldLabel: 'Remember me', name: 'remember' }
            ],
            
            buttons: [
                {text:'Register'},
                {
                	text:'Login',
                	handler: function(){
                		//var cmp = Ext.getCmp("chenfuzhi");
                		//console.log(cmp);
                		this.up('a123').removeAll();
                		//this.up('Main').
                	}
                }
            ]
        }
    ]
});
