Ext.define('KitchenSink.store.Task', {
    extend: 'Ext.data.TreeStore',
    autoload: true,
    //fields: ['text'],
    proxy:{
    	type:'ajax',
		url: '/api/tree/'
    },
	listeners: {
	   beforeload: {
	   		fn: function(store, operation, eOpts ){
	   			if(operation.node.raw.id == "root"){
           			var root = store.getRootNode();
           			root.appendChild([
				            {
				                text: 'forms',
				                id	: 'task',
				                find: '{"type":"task"}'
				            },
				            {
				                text: 'grids',
				                id	: 'rule',
				                find: '{"type":"rule"}'
				            }
				    ]);
					
           			console.log(store.getProxy().self.getName());
           			return false;
           		}
           		
	   		}
	   }
       
	}
});
