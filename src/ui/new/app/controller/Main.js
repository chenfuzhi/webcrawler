Ext.define('KitchenSink.controller.Main', {
    extend: 'Ext.app.Controller',
    stores: [
    	'Task',
        'Examples',
        'Companies',
        'Restaurants',
        'States',
        'TreeStore'
    ],

    views: [
        'Viewport',
        'Header'
    ],

    refs: [
        {
            ref: 'examplePanel',
            selector: '#examplePanel'
        },
        {
            ref: 'exampleList',
            selector: 'exampleList'
        }
    ],

    init: function() {

	    var menuC = Ext.create('Ext.menu.Menu', {
	    				id:'menu0011',
					    width: 100,
					    height: 100,
					    items: [{
					        text: 'icon item',
					    },{
					        text: 'text item'
					    },{
					        text: 'plain item',
					    }]
					    
					});
	    var btn =	Ext.create('Ext.Button', {
						    text: '+',
						    //iconCls: 'add16',
						    handler: function() {
						    	
						    	
						    	var store = Ext.getCmp('exampleList').getStore();
						    	store.getNodeById('task').appendChild({text:'chenfuzhi', id:'5555', leaf:true});
						    	
						    }
					});


        this.control({
            'viewport exampleList': {
            	"itemcontextmenu":function(tree, node, item, index, e, eOpts){
			    	//menuC.show(item);
			    	menuC.showAt(e.getXY());
			    	e.stopEvent();
			    },
            	"itemclick":function(tree, record, item, index, e, eOpts ){
			    	
			    	//console.log(record);
			    	if(record.raw.id == 'task'){
			    		if(btn.rendered == false){
							btn.render(item);
			    		}
			    	}
			    	//console.log(item);
			    },
                'select': function(me, record, index, e) {
                    if (!record.isLeaf()) {
                        return;
                    }
					var log = Ext.create('KitchenSink.view.widgets.Login');
					//log.show(record);
					log.show(this);
                },
                afterrender: function(){
                    var me = this,
                        className, exampleList, name, record;

                    setTimeout(function(){
                        className = location.hash.substring(1);
                        exampleList = me.getExampleList();

                        if (className) {
                            name = className.replace('-', ' ');
                            record = exampleList.view.store.find('text', name);     
                        } else {
							record = exampleList.view.store.find('text', 'grouped header grid');
						}

                        //exampleList.view.select(record);
                    }, 0);
                }
            }
        });
    }

});
