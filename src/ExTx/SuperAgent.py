#coding: utf-8
###############################################################################
##
##  Copyright 2011 Tavendo GmbH
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##      http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##
###############################################################################

import sys
import os
import os
import urllib
from twisted.web.client import Agent, ContentDecoderAgent, GzipDecoder

from twisted.python import log
from twisted.web import http
from twisted.internet import reactor
from twisted.web import error
from twisted.web._newclient import ResponseDone, ResponseFailed
from twisted.python import failure
from twisted.python.failure import Failure

from twisted.internet.defer import Deferred, \
                                   DeferredList, \
                                   gatherResults, \
                                   returnValue, \
                                   inlineCallbacks
import redis
import pymongo
from bson.objectid import ObjectId

from lxml import etree
from scrapy.selector import HtmlXPathSelector
from scrapy.http import Response, HtmlResponse
from scrapy.http.response.text import TextResponse

import urllib2, gzip, StringIO

from twisted.web.client import Agent, HTTPConnectionPool, ProxyAgent, RedirectAgent, CookieAgent, _parse
from twisted.web.http_headers import Headers
from twisted.web.client import FileBodyProducer
from twisted.internet.endpoints import TCP4ClientEndpoint
from cookielib import CookieJar


from twisted.internet.ssl import ClientContextFactory
from twisted.internet.protocol import Protocol

import time


class SuperAgent(object):
    '''
    
    
    '''
    def __init__(self, endpoint=None, redirectLimit=20, decoders=[], pool=HTTPConnectionPool(reactor)):
        '''
        
        :param agent: Agent, proxyAgent
        :param decoders:
        '''
        self._endpoint = endpoint
        if self._endpoint is not None:#代理
            self._agent = ProxyAgent(endpoint, pool=pool)
        else:
            self._agent = Agent(reactor, pool=pool)
            
            
        self._decoders = dict(decoders)
        self._supported = ','.join([decoder[0] for decoder in decoders])
        self._redirectLimit = redirectLimit


    def request(self, method, uri, headers=None, bodyProducer=None):
        """
        Send a client request which declares supporting compressed content.

        @see: L{Agent.request}.
        """
        if headers is None:
            headers = Headers()
        else:
            headers = headers.copy()
        
        if len(self._supported) > 0:
            headers.addRawHeader('accept-encoding', self._supported)
            
        d = self._agent.request(method, uri, headers, bodyProducer)
        
        
        if self._redirectLimit != 0:
            d.addCallback(
                self._handleRedirectResponse, method, uri, headers, 0) #跳转
        
        if len(self._supported) > 0:
            d.addCallback(self._handleResponse) # 解码gzip

        return d



    def _handleResponse(self, response):
        """
        Check if the response is encoded, and wrap it to handle decompression.
        """
        contentEncodingHeaders = response.headers.getRawHeaders(
            'content-encoding', [])
        contentEncodingHeaders = ','.join(contentEncodingHeaders).split(',')
        while contentEncodingHeaders:
            name = contentEncodingHeaders.pop().strip()
            decoder = self._decoders.get(name)
            if decoder is not None:
                response = decoder(response)
            else:
                # Add it back
                contentEncodingHeaders.append(name)
                break
        if contentEncodingHeaders:
            response.headers.setRawHeaders(
                'content-encoding', [','.join(contentEncodingHeaders)])
        else:
            response.headers.removeHeader('content-encoding')
        return response
    
    
    def _handleRedirectResponse(self, response, method, uri, headers, redirectCount):
        """
        Handle the response, making another request if it indicates a redirect.
        """
        if response.code in (http.MOVED_PERMANENTLY, http.FOUND,
                             http.TEMPORARY_REDIRECT):
            if method not in ('GET', 'HEAD'):
                err = error.PageRedirect(response.code, location=uri)
                raise ResponseFailed([failure.Failure(err)], response)
            return self._handleRedirect(response, method, uri, headers,
                                        redirectCount)
        elif response.code == http.SEE_OTHER:
            return self._handleRedirect(response, 'GET', uri, headers,
                                        redirectCount)
        return response


    def _handleRedirect(self, response, method, uri, headers, redirectCount):
        """
        Handle a redirect response, checking the number of redirects already
        followed, and extracting the location header fields.
        """
        if redirectCount >= self._redirectLimit:
            err = error.InfiniteRedirection(
                response.code,
                'Infinite redirection detected',
                location=uri)
            raise ResponseFailed([failure.Failure(err)], response)
        locationHeaders = response.headers.getRawHeaders('location', [])
        if not locationHeaders:
            return response
        
        location = locationHeaders[0]
        deferred = self._agent.request(method, location, headers)
        return deferred.addCallback(
            self._handleRedirectResponse, method, uri, headers, redirectCount + 1)





class SuperProxyAgent(object):
    '''
    proxy:
    decoder:
    redirect:
    cookie:-> headers
    
    
    '''
    def __init__(self, agent, decoders):
        self._agent = agent
        self._decoders = dict(decoders)
        self._supported = ','.join([decoder[0] for decoder in decoders])


    def request(self, method, uri, headers=None, bodyProducer=None):
        """
        Send a client request which declares supporting compressed content.

        @see: L{Agent.request}.
        """
        if headers is None:
            headers = Headers()
        else:
            headers = headers.copy()
        headers.addRawHeader('accept-encoding', self._supported)
        
        

        key = ("http-proxy", self._agent._proxyEndpoint)


        d = self._agent._requestWithEndpoint(key, self._agent._proxyEndpoint, method,
                                             _parse(uri), headers, bodyProducer,
                                             uri)
        
        return d.addCallback(self._handleResponse)


    def _handleResponse(self, response):
        """
        Check if the response is encoded, and wrap it to handle decompression.
        """
        contentEncodingHeaders = response.headers.getRawHeaders(
            'content-encoding', [])
        contentEncodingHeaders = ','.join(contentEncodingHeaders).split(',')
        while contentEncodingHeaders:
            name = contentEncodingHeaders.pop().strip()
            decoder = self._decoders.get(name)
            if decoder is not None:
                response = decoder(response)
            else:
                # Add it back
                contentEncodingHeaders.append(name)
                break
        if contentEncodingHeaders:
            response.headers.setRawHeaders(
                'content-encoding', [','.join(contentEncodingHeaders)])
        else:
            response.headers.removeHeader('content-encoding')
        return response

class SuperDrectProxyAgent(object):
    '''
    proxy:
    decoder:
    redirect:
    cookie:-> headers
    
    
    '''
    def __init__(self, agent, decoders):
        self._agent = agent
        self._decoders = dict(decoders)
        self._supported = ','.join([decoder[0] for decoder in decoders])
        self._redirectLimit = 20


    def request(self, method, uri, headers=None, bodyProducer=None):
        """
        Send a client request which declares supporting compressed content.

        @see: L{Agent.request}.
        """
        if headers is None:
            headers = Headers()
        else:
            headers = headers.copy()
        headers.addRawHeader('accept-encoding', self._supported)
        
        

        key = ("http-proxy", self._agent._proxyEndpoint)


        d = self._agent._requestWithEndpoint(key, self._agent._proxyEndpoint, method,
                                             _parse(uri), headers, bodyProducer,
                                             uri)
        
        d.addCallback(
            self._handleRedirectResponse, method, uri, headers, 0) #跳转
        
        
        d.addCallback(self._handleResponse) # 解码

        return d



    def _handleResponse(self, response):
        """
        Check if the response is encoded, and wrap it to handle decompression.
        """
        contentEncodingHeaders = response.headers.getRawHeaders(
            'content-encoding', [])
        contentEncodingHeaders = ','.join(contentEncodingHeaders).split(',')
        while contentEncodingHeaders:
            name = contentEncodingHeaders.pop().strip()
            decoder = self._decoders.get(name)
            if decoder is not None:
                response = decoder(response)
            else:
                # Add it back
                contentEncodingHeaders.append(name)
                break
        if contentEncodingHeaders:
            response.headers.setRawHeaders(
                'content-encoding', [','.join(contentEncodingHeaders)])
        else:
            response.headers.removeHeader('content-encoding')
        return response
    
    
    def _handleRedirectResponse(self, response, method, uri, headers, redirectCount):
        """
        Handle the response, making another request if it indicates a redirect.
        """
        if response.code in (http.MOVED_PERMANENTLY, http.FOUND,
                             http.TEMPORARY_REDIRECT):
            if method not in ('GET', 'HEAD'):
                err = error.PageRedirect(response.code, location=uri)
                raise ResponseFailed([failure.Failure(err)], response)
            return self._handleRedirect(response, method, uri, headers,
                                        redirectCount)
        elif response.code == http.SEE_OTHER:
            return self._handleRedirect(response, 'GET', uri, headers,
                                        redirectCount)
        return response


    def _handleRedirect(self, response, method, uri, headers, redirectCount):
        """
        Handle a redirect response, checking the number of redirects already
        followed, and extracting the location header fields.
        """
        if redirectCount >= self._redirectLimit:
            err = error.InfiniteRedirection(
                response.code,
                'Infinite redirection detected',
                location=uri)
            raise ResponseFailed([failure.Failure(err)], response)
        locationHeaders = response.headers.getRawHeaders('location', [])
        if not locationHeaders:
            return response
        
        location = locationHeaders[0]
        deferred = self._agent.request(method, location, headers)
        return deferred.addCallback(
            self._handleRedirectResponse, method, uri, headers, redirectCount + 1)

#
#
#
#



def main():
    contextFactory = WebClientContextFactory()
    agent = Agent(reactor, contextFactory)
    d = agent.request("GET", "https://example.com/")
    d.addCallbacks(display, err)
    d.addCallback(lambda ignored: reactor.stop())
    reactor.run()
    
    
class BeginningPrinter(Protocol):
    def __init__(self, finished):
        self.finished = finished
        self.remaining = 1024 * 10
        self.body = ""

    def dataReceived(self, bytes):
        self.body += bytes

    def connectionLost(self, reason):
        print 'Finished receiving body:', reason.getErrorMessage()
        self.finished.callback(self.body)
        
        
@inlineCallbacks
def get_success(response):
    print "get_success", response
    finished = Deferred()
    response.deliverBody(BeginningPrinter(finished))
    ret = yield finished
    print "result:%s" % ret

def get_error(result):
    print "get_error", result
    pass






if __name__ == '__main__':

   log.startLogging(sys.stdout)

#   print urllib.urlopen("http://www.wealink.com").read()
   
#   time.sleep(10)
   args = {
           "encoding": "utf-8",
           "url" : "http://iframe.ip138.com/ic.asp",
           "data" : {
                      "a1":'//title',
                     },
           "cookie":{
                    #"uid":"5000890",
                    #"username":"chenwangang1111@163.com",
                    #"password":"111111",
                    "PHPSESSID":"6bsn39mc9pbafj17og4udbl0c6"
                    },
           }

   proxy = {
            "host":"192.168.2.202",
            "port":3128
            }

   endpoint = TCP4ClientEndpoint(reactor, "192.168.2.202", 3128)
   agent = ProxyAgent(endpoint)

#   agent = SuperProxyAgent(agent, [('gzip', GzipDecoder)])
#      ContentDecoderAgent
#   agent = Agent(reactor)

   agent = SuperAgent(endpoint=None, redirectLimit=3, decoders=[])
   d = agent.request("GET", "http://www.baidu.com")
   d.addCallbacks(get_success, get_error)
   
   
#   d.addCallback(lambda ignored: reactor.stop())
   reactor.run()
