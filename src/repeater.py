#coding: utf-8
###############################################################################
##
##  Copyright 2011,2012 Tavendo GmbH
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##      http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##
###############################################################################

import sys, math

from twisted.python import log
from twisted.internet import reactor, defer
from twisted.web.server import Site
from twisted.web.static import File

from autobahn.websocket import listenWS
from autobahn.wamp import exportSub, \
                          exportPub, \
                          exportRpc, \
                          WampServerFactory, \
                          WampServerProtocol

import json
from bson.objectid import ObjectId



class RepeaterServerProtocol(WampServerProtocol):
        
    def onOpen(self):
        self.factory.register(self)
        WampServerProtocol.onOpen(self)
        
        
    def connectionLost(self, reason):
        WampServerProtocol.connectionLost(self, reason)
        self.factory.unregister(self)
        
    def onMessage(self, msg, binary):
        WampServerProtocol.onMessage(self, msg, binary)
        
    def onSessionOpen(self):
        self.registerProcedureForRpc("clients", self.getClients)
        self.registerForPubSub("spider")
        self.registerForPubSub("tasker")
        
        self.registerForPubSub("admin")
        
        log.msg("on connection sid:%s, peer:%s" % (self.session_id, self.peerstr))
        
        self.peerstr
        
        
    def getClients(self, channel):
        rets = []
        if self.factory.subscriptions.has_key(channel):
            for proto in self.factory.subscriptions[channel]:
                val = {}
                val.setdefault("sid", proto.session_id)
                val.setdefault("peer", proto.peerstr)
                
                rets.append(val)
        return rets
        
      
class StoreServerFactory(WampServerFactory):

    def __init__(self, url, debugWamp = False):
       WampServerFactory.__init__(self, url, debugWamp = debugWamp)
       self.clients = []
       
    def onClientSubscribed(self, proto, topicUri):
        self.bcClients(proto, topicUri, "open")
        
    def onClientUnsubscribed(self, proto, topicUri):
        self.bcClients(proto, topicUri, "close")

    def bcClients(self, proto, topicUri, action):
        '''
            蜘蛛或任务下线。或上线。广播通知
        '''
        msg = {"msg_type":"broadcast",
               "action": action,
               "topicUri": topicUri,
               "session_id": proto.session_id
               }
        
        pub_channel = ""        
        if topicUri == "spider":
            pub_channel = "tasker"
            
            self.dispatch("admin", msg)   #通知管理界面蜘蛛上线或下线
            
        elif topicUri == "tasker":
            pub_channel = "spider"
        else:
            return
            
        if self.subscriptions.has_key(pub_channel):
            print "通知对方下线或上线", msg, "频道：%s" % pub_channel
            
            self.dispatch(pub_channel, msg)
        
        
    def register(self, client):
        if not client in self.clients:
            print "registered client " + client.peerstr
            self.clients.append(client)
        
    def unregister(self, client):
        if client in self.clients:
            print "unregistered client " + client.peerstr
            self.clients.remove(client)
              

from test import test_init_data

if __name__ == '__main__':
    import sys
    import os
#    reset = raw_input("reset localhost, 27017 database: y/n:")
#    if reset == "y":
#        test_init_data.initData("localhost", 27017)
#    
    log.startLogging(sys.stdout)
    factory = StoreServerFactory("ws://localhost:9000", debugWamp = True)
    factory.protocol = RepeaterServerProtocol
    factory.setProtocolOptions(allowHixie76 = True)
    listenWS(factory)
    reactor.run()



