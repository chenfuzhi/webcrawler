# -*- coding: utf-8 -*-
###############################################################################
##
##  Copyright 2011 Tavendo GmbH
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##      http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##
###############################################################################

import sys
from twisted.python import log
from twisted.internet import defer, reactor
from twisted.internet.defer import Deferred, \
                                   DeferredList, \
                                   gatherResults, \
                                   returnValue, \
                                   inlineCallbacks
from autobahn.websocket import connectWS
from autobahn.wamp import WampClientFactory, WampClientProtocol
import redis
import pymongo
from bson.objectid import ObjectId
import json
from Queue import Queue
import time
from ExTx.json_util import object_hook, default
import setting



class TaskClientProtocol(WampClientProtocol):
    """
    Demonstrates simple Publish & Subscribe (PubSub) with Autobahn WebSockets.
    """

    def connectionLost(self, reason):
        WampClientProtocol.connectionLost(self, reason)
        self.connected = 0
        
        #重置缓存队列： ready
        for k, v in self.cache.iteritems():
            self.f_redis.resetLinkdbStatus(v["data"]["url_id"])
        self.cache = {}
        
        
    @defer.inlineCallbacks
    def refreshSpiders(self, result):
        
            
        self.spiders = result
            
        #在缓存队列里面的无对应蜘蛛ID的重置为： ready
        popks = []
        for k, v in self.cache.iteritems():
            if v["spider"] not in self.spiders:
                yield self.f_redis.resetLinkdbStatus(v["data"]["url_id"])
                popks.append(k)
        for k in popks:
            self.cache.pop(k)
            
        log.msg("刷新蜘蛛列表：")
        print self.spiders
        
    def getOneSpider(self):
        self.sel_spider += 1
        index = self.sel_spider % len(self.spiders)
        return self.spiders[index]["sid"]
        
        
    @defer.inlineCallbacks   # 这个有个问题 当中继器断开，重连接得时候。会重新执行这个初始化函数
    def onSessionOpen(self):
        self.f_mongo = self.factory.mongo
        self.f_redis = self.factory.redis
        
        
        self.subscribe("tasker", self.process_msg)
        log.msg("订阅tasker完成。。")
        
        
        self.spiders = {}
        self.conf = yield self.f_mongo.getTaskConf(self.factory.task_id)
        
        print json.dumps(self.conf, indent=2, default=default)
        
        self.sel_spider = 0
        self.cache = {} # 缓存
        self.errs = {}
        self.publish_finish = False
        self.tmp = {}
        self.spider_runner = {}  # 运行时设置每个蜘蛛并发数量

        if self.factory.is_restart == "yes":
            log.msg("task has reinit now...")
            time.sleep(3)
            red = redis.Redis("localhost", 6379)
            red.flushall()
            yield self.f_redis.initTask(self)  # 初始化起始地址
        log.msg("initTask finish。。")
        
        self.call("clients", "spider").addCallback(self.refreshSpiders)
        
        reactor.callLater(3, self.running)
        
    
    def running(self):
        if not self.connected: 
            log.msg("已经失去连接， 等待重新连接")
            return
        
        if len(self.spiders) == 0:
            reactor.callLater(3, self.running)
            log.msg("没有可用的蜘蛛，等待3秒")
        else:
            if len(self.cache) < 1:
                self.make_next_crawl()
            else:
                log.msg("已经达到最大的发送队列, 等待3秒")
                reactor.callLater(3, self.running)
    
    def getEncoding(self, link_id):
        for l in self.conf["linkdbs"]:
            if l["id"] == link_id:
                return l["encoding"]
    
    
    @defer.inlineCallbacks
    def make_next_crawl(self):
        '''
        得到下一条抓取指令
        '''
        spider_id = self.getOneSpider()
        link = yield self.f_redis.getReadyLink(spider_id)
        if link:
            link = json.loads(link)
            
            log.msg("取得link:%s" % str(link))
            
            crawl_cmd = {}
            crawl_cmd.setdefault("url_id", link["url_id"])
            crawl_cmd.setdefault("url", link["url"])
            crawl_cmd.setdefault("encoding", self.conf["linkdbs"][link["linkdb_id"]]["encoding"])
            
            if self.conf.has_key("cookie"):
                crawl_cmd.setdefault("cookie", self.conf["cookie"])
            else:
                crawl_cmd.setdefault("cookie", {})
            
            extrs = []
            for e_id in self.conf["navi"][link["linkdb_id"]]["extrs"]:
                extrs.append(self.conf["extrs"][e_id])
                
            crawl_cmd.setdefault("extrs", extrs)
            
            
            msg = {
                   "pid"  : str(ObjectId()),
                   "spider": spider_id,
                   "msg_type": "crawl",
                   "crawl_type" : "http", #phantomjs
                   "data" : crawl_cmd,
                   "tasker" : self.session_id,
                   "time" : int(time.time())
                   }
            
            log.msg("生成一个抓取指令:%s,地址：%s" % (msg["pid"], link["url"].encode("utf-8")))
            self.publish("spider", msg, eligible=[msg["spider"]])
            self.cache.setdefault(msg["pid"], msg)
            reactor.callLater(self.conf["delay"], self.running)
        else:
            reactor.callLater(3, self.running)
            log.msg("暂时没有可抓取的链接了, 等待3秒")
            

    def save_finish_url(self, url_info):
        print "save_finish_url: %s, type: %s" % (url_info, type(url_info)) 
        self.f_mongo.store_finish_url(url_info)
        
        
    def process_msg(self, topicUri, event):
        log.msg("收到消息：%s" % str(event))
        
        if event["msg_type"] == "response":
            if self.cache.has_key(event["pid"]):
                self.cache.pop(event["pid"])
                
            if event["success"] == 1:
                
                self.f_redis.delete_finish_url(event["url_id"], self.save_finish_url)
                
                
                for extr in event["extrs"]:
                    if extr["extr_type"] == "item":
                        self.f_redis.delete_finish_url(event["url_id"], self.save_finish_url)
                        self.f_mongo.store_item_data(extr["data"], extr["store_data"])
                        
                    elif extr["extr_type"] == "link":
                        for l in extr["data"]["link"]:
                            if not l.startswith("http"):
                                l = "".join([self.conf["siteurl"], l])
                                
                            item_lk = {
                                    "url_id": str(ObjectId()),
                                    "linkdb_id": extr["store_linkdb"],
                                    "url"  : l,
                                    "refere"  : event["crawl_url"],
                                    "time"  :  int(time.time()),
                                    }
                            
                            self.f_redis.store_link(item_lk)
            elif event["success"] == 0:
                log.msg("收到消息")
                
                if event.has_key("status") and event["status"] == "404":
                    self.f_redis.delete_finish_url(event["url_id"], self.save_finish_url)
                else:
                    self.f_redis.resetLinkdbStatus(event["url_id"])
                    

        elif event["msg_type"] == "broadcast":
            print event
            self.call("clients", "spider").addCallback(self.refreshSpiders)
        



class TaskClientFactory(WampClientFactory):
    def __init__(self, url, debug = False, debugCodePaths = False, debugWamp = False, debugApp = False, **conn):
        WampClientFactory.__init__(self, url, debug = debug, debugCodePaths = debugCodePaths)
        self.redis = conn["redis"]
        self.mongo = conn["mongo"]
        
        self.task_id = conn["task_id"]
        self.is_restart = conn["restart"]
        
    
    def startFactory(self):
        print "startFactory"
        pass

        
        
    def clientConnectionFailed(self, connector, reason):
        log.msg("连接失败, 3秒后重试")
        reactor.callLater(3, connector.connect)
        
        
    def clientConnectionLost(self, connector, reason):
        log.msg("连接已经失去, 3秒后重试")
        reactor.callLater(3, connector.connect)


from lib.IRedis import redisDB
from lib.IMongodb import mongoDB
import sys


@defer.inlineCallbacks
def init_dbs(setting):
    redis = redisDB()
    mongo = mongoDB()
    yield redis.connect(setting)
    yield mongo.connect(setting)

    redis.key_linkdb = "%s:linkdb" % sys.argv[1]
    redis.key_ready = "%s:ready" % sys.argv[1]
    redis.key_running = "%s:running" % sys.argv[1]
    
    
    conn = {
            "redis": redis,
            "mongo": mongo,
            "task_id": str(sys.argv[1]),
            "restart": str(sys.argv[2])
            }
    log.msg("redis, mongo 链接成功。")
    factory = TaskClientFactory("ws://localhost:9000", **conn)
    factory.protocol = TaskClientProtocol
    connectWS(factory)
    
    
    
if __name__ == '__main__':
    
    if len(sys.argv) < 3:
        print "please input 2 args:"
        print "eg: python tasker.py XXXXXXXXXXXXXXX yes/no"
        sys.exit()
    
        
    log.startLogging(sys.stdout)
    init_dbs(setting)
    reactor.run()

   
   
   
