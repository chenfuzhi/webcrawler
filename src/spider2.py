#coding: utf-8
###############################################################################
##
##  Copyright 2011 Tavendo GmbH
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##      http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##
###############################################################################

import sys
import os

from twisted.python import log
from twisted.internet import reactor
from twisted.web.error import Error
from twisted.internet.defer import Deferred, \
                                   DeferredList, \
                                   gatherResults, \
                                   returnValue, \
                                   inlineCallbacks
from autobahn.websocket import connectWS
from autobahn.wamp import WampClientFactory, WampClientProtocol
import redis
import pymongo
from bson.objectid import ObjectId
from twisted.web.client import getPage

from lxml import etree
from scrapy.selector import HtmlXPathSelector
from scrapy.http import Response, HtmlResponse
from scrapy.http.response.text import TextResponse

import urllib2, gzip, StringIO
from ExTx.SuperAgent import SuperAgent, BeginningPrinter
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.web.http_headers import Headers

import time
from autobahn.util import newid

print newid()

def xpathExtract(body, encoding, url, extr={}):

    try:
        body = gzip.GzipFile(fileobj=StringIO.StringIO(body)).read()
    except:
        pass
        
    _res = TextResponse(url=url, body=body, encoding=encoding)
    
    hxs = HtmlXPathSelector(_res)
    
    data = {}
    for k, v in extr.iteritems():
        dl = []
        for d in hxs.select(v):
            dl.append(d.extract())
#        if len(dl) == 1:
#            dl = dl[0]
        if k != "link":
            data[k] = "".join(dl)
        else:
            data[k] = dl
        
    data["siteurl"] = url
    data["timestamp"] = time.time()
    data["time"] = time.strftime("%Y-%m-%d %H:%I:%S", time.localtime())
    
    return data




class TaskClientProtocol(WampClientProtocol):
    """
    Demonstrates simple Publish & Subscribe (PubSub) with Autobahn WebSockets.
    """
    
    def crawl_error(self, result, **kw):
        '''
        self.status = code
        self.message = message
        self.response = response
        
        '''
        
        print "错误:", str(result)
        
        res = {}
        res = {
               "pid"    : kw["pid"],
               "success": 0,
               "extrs"  : [],
               "msg_type" : "response",
               "tasker" : kw["tasker"],
               "spider" : kw["spider"],
               "time"   : int(time.time()),
               "url_id" : kw["data"]["url_id"],
               "crawl_type" : kw["crawl_type"],
               }
#        print "isinstance", isinstance(result, Exception), type(result)
        try:
            print result.getErrorMessage(), result.type, result.value, result.captureVars
        except:
            print "aaaaaa, error"
        
        if isinstance(result.value, Error):
            res["status"] = result.value.status
            res["message"] = result.value.message
            #res["response"] = result.value.response
            
        
        self.publish("tasker", 
                     res,
                     eligible=[res["tasker"]])
        
        log.err("错误：%s" % str(res))
    
    @inlineCallbacks
    def crawl_success(self, response, **kw):
        '''
        '''
        finished = Deferred()
        response.deliverBody(BeginningPrinter(finished))
        result = yield finished
        
        extrs = []
        for extr in kw["data"]["extrs"]:
            info = {}
            info["extr_type"] = extr["extr_type"]
            info["store_linkdb"] = extr.get("store_linkdb")
            info["store_data"] = extr.get("store_data")
            info["data"] = {}
            
            _extr = {}
            for k, v in extr["data"].iteritems():
                _extr.setdefault(k, v.encode("utf-8"))
            
            info["data"] = xpathExtract(result, kw["data"]["encoding"], kw["data"]["url"], extr=_extr)
            extrs.append(info)

        res = {}
        res = {
               "pid"    : kw["pid"],
               "success": 1,
               "extrs"  : extrs,
               "msg_type"   : "response",
               "tasker" : kw["tasker"],
               "spider" : kw["spider"],
               "time"   : int(time.time()),
               "crawl_url": kw["data"]["url"],
               "url_id" : kw["data"]["url_id"],
               "spider_type" : kw["crawl_type"],
               }
                    
        self.publish("tasker", 
                     res,
                     eligible=[kw["tasker"]])
        
        log.msg("抓取成功结束:%s" % str(res))
        
    def process_msg(self, topic, event):
        
        log.msg("topic:%s, event: %s" % (str(topic), str(event)))
        if event["msg_type"] == "crawl":
            if event["crawl_type"] == "http":
                data = event["data"]
                
                cookies = {}
                for k, v in data["cookie"].iteritems():
                    cookies.setdefault(k.encode("utf-8"), v.encode("utf-8"))
                    
                headers = {
                           "Accept-Charset": "GBK,utf-8;q=0.7,*;q=0.3",
                           "Connection": "keep-alive",
                           "Cache-Control": "max-age=0",
                           #"User-Agent": "Baiduspider",
                           "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11 AlexaToolbar/alxg-3.1",          
                           "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                           "Accept-Encoding": "gzip,deflate,sdch",
                           "Accept-Language": "zh-CN,zh;q=0.8",
                           "Accept-Charset": "GBK,utf-8;q=0.7,*;q=0.3",
                           }

                proxy = {
                         "host" : "192.168.2.202",
                         "port" : 3128
                         }
                
                h = Headers()
                for k, v in headers.iteritems():
                    h.addRawHeader(k, v)
                
                cookieData = []
                for cookie, cookval in cookies.items():
                    cookieData.append('%s=%s' % (cookie, cookval))
                if cookieData:
                    h.addRawHeader('Cookie', '; '.join(cookieData))
                    
                
                endpoint = TCP4ClientEndpoint(reactor, "192.168.2.202", 3128)

                agent = SuperAgent(endpoint=endpoint, redirectLimit=3, decoders=[])
                
                f1 = open("/tmp/%s.req" % str(ObjectId()), 'w')
                f1.write("url:%s \n" % data["url"])
                for k, v in h.getAllRawHeaders():
                    f1.write("%s:%s\n" % (k, v[0]))
                                
                f1.close()
                
                d = agent.request("GET", data["url"].encode("utf-8"), headers=h)
                d.addCallbacks(
                              self.crawl_success,
                              self.crawl_error,
                              callbackKeywords=event,
                              errbackKeywords=event
                              )
                log.msg("一条url异步访问开始")
            elif event["crawl_type"] == "phantomjs":
                pass
            
            
        elif event["msg_type"] == "broadcast":
            pass

            
    def onSessionOpen(self):
        log.msg("peerstr: %s" % self.peerstr)
        self.subscribe("spider", self.process_msg)
        log.msg("订阅spider完成。。")
        
      
class SpiderClientFactory(WampClientFactory):
    def __init__(self, url, debug = False, debugCodePaths = False, debugWamp = False, debugApp = False):
        WampClientFactory.__init__(self, url, debug = debug, debugCodePaths = debugCodePaths)

        
    def clientConnectionFailed(self, connector, reason):
        log.msg("连接失败, 3秒后重试")
        reactor.callLater(3, connector.connect)
        
        
    def clientConnectionLost(self, connector, reason):
        log.msg("连接已经失去, 3秒后重试")
        reactor.callLater(3, connector.connect)



if __name__ == '__main__':

   log.startLogging(sys.stdout)
   factory = SpiderClientFactory("ws://localhost:10000")
   factory.protocol = TaskClientProtocol
   connectWS(factory)
   reactor.run()
