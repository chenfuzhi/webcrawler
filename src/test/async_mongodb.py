
import txmongo
from twisted.internet import defer, reactor

conn = None

def recall():
    conn.store["51jobs"].find(limit=10).addCallback(find_end)


def find_end(result):
    #reator.callLater(2, recall)
    print result
    reactor.callLater(2, recall)

def connection_end(result):
    global conn
    conn = result
    test = result.store["51jobs"]
    
    print type(test)
    # fetch some documents
    return test.find(limit=10)
    
def main():
    txmongo.MongoConnection().addCallback(connection_end).addCallback(find_end)
    # fetch some documents
    
    
if __name__ == "__main__":
            
    reactor.callLater(0, main)
    reactor.run()