import txredisapi as redis
import cyclone.web
import cyclone.redis
from twisted.internet import defer
from twisted.internet import reactor

conn = None


@defer.inlineCallbacks
def main():
    rc = yield redis.Connection()
    print rc

    yield rc.set("foo", "bar")
    v = yield rc.get("foo")
    print "foo:", repr(v)

    yield rc.disconnect()

@defer.inlineCallbacks
def getRedis():
    print conn
    ret = yield conn.get("foo")
    print ret
    

@defer.inlineCallbacks
def redis_conn():
    conn1  = yield cyclone.redis.ConnectionPool()
    conn2  = yield cyclone.redis.ConnectionPool()
    
    print conn1, conn2
    


if __name__ == "__main__":
    redis_conn()
    reactor.run()