#coding: utf-8


from bson.objectid import ObjectId

def newid():
    return str(ObjectId())



accounts_fenzhi = {
                 "_id": ObjectId(),
                 "alias": "分智帐号",
                 "username": "chenfuzhi",
                 "password": "chenfuzhi",
                 "cookie": "",
                 "timestamp": 0,
                 "maxuse": 0 
                 }


linkdb_fenzhi = { 
               "id"    : str(ObjectId()), 
               "alias"  : "公司列表",
               "encoding":"gbk",
               "prio"   : 0,
               "weight" : 500,
               "login":{
                        "account": str(accounts_fenzhi["_id"]),
                        "no_loginflag" : {
                                          "check": "exists/contain/equel/",
                                          "text" "你还没登陆"
                                          "extr" : '//table[@class="jobs_1"]//td[@width="70%"]/a/text()',
                                          } 
                                        
                        }
               }



"""

"""
linkdb1 = { 
           "id"    : str(ObjectId()), 
           "alias"  : "地区列表",
           "encoding":"gbk",
           "prio"   : 0,
           "weight" : 500

           }
linkdb2 = {
           "id"    : str(ObjectId()), 
           "alias"  : "列表地址",
           "encoding":"gbk",
           "prio"   : 0,
           "weight" : 500
           }
linkdb3 = { 
           "id"    : str(ObjectId()), 
           "alias"  : "职位地址",
           "encoding":"gbk",
           "prio"   : 1,
           "weight" : 500,
           "duplicate": "url", #去重策略， url,size
           "crawl"  : False,   #是否可以抓取
           "spider_peer": "192.168.2.202"
           }

extr1 = {
         "id"    : str(ObjectId()), 
         "alias"    : "职位信息",
         "extr_type"   : "item",  #content, link
         "data"   : {
                     "name"     :'//table[@class="jobs_1"]//td[@width="70%"]/a/text()',
                     "title"    :'//table[@class="jobs_1"]//td[@class="sr_bt"]/text()'
                     },
         
         "store_data"  : "fenzhi:51jobs"
         
         }
extr2 = {
           "id"    : str(ObjectId()), 
           "alias"      : "提取地区地址",
           "extr_type"   : "link",  #content, link
           
           "data"   :{
                      "link":'//*[@id="typeSearchTbl2"]//td[@style="padding-left:2px;"]/a/@href',
                      },
           
           "store_linkdb"  : linkdb2["id"]
          }

extr3 = {
            "id"    : str(ObjectId()), 
            "alias" : "提取下一页",
            "extr_type"   : "link",  #content, link
           
            "data"   :{
                      "link":'//table[@class="navBold"]//tr[1]/td[3]/a/@href',
                      },
            "store_linkdb"  : linkdb2["id"]
         }

extr4 = {
            "id"    : str(ObjectId()), 
            "alias"  : "提取职位详细页地址",
            "extr_type"   : "link",  #content, link
           
            "data"   :{
#                       "timestamp": "",
                       "link":'//table[@id="resultList"]/tr[@class="tr0"]//a[@class="jobname"]/@href',
#                       "Referer": "",
#                       "size": 1111
                      },
         
            "store_linkdb"  : linkdb3["id"]
         }

# task1测试用得任务
task1 = {
          "_id"    : ObjectId(), 
          "alias"    :  "51job",
          "siteurl"  : "http://www.51job.com",
          "navi"    : {
                           linkdb1["id"]   :   {"extrs":[extr2["id"]],
                                                "enable": 1,
                                                "prio": 1,
                                                "weight": 500
                                                },
                           
                           linkdb2["id"]   :   {"extrs":[extr3["id"], extr4["id"]],
                                                "enable": 1,
                                                "prio": 1,
                                                "weight": 500
                                                },
                           linkdb3["id"]   :   {"extrs":[extr1["id"]],
                                                "enable": 1,
                                                "prio": 1,
                                                "weight": 500
                                                }
                       },
         
          

          "start_links"  :[
                           {
                            "url_id": str(ObjectId()),
                            "linkdb_id":linkdb1["id"],
                            "url":"http://search.51job.com/jobsearch/advance_search.php?lang=c&stype=2",
                            "url_type": "static",  #static, increase
                            "refere":"",
                            "encoding": "gbk",
                            "status":"ready",
                           }
                           ],
          "run_times":  0,
          "delay" : 0.01,
          "linkdbs":{
                     linkdb1["id"]:linkdb1,
                     linkdb2["id"]:linkdb2,
                     linkdb3["id"]:linkdb3
                     },
         
         "extrs":{
                  extr1["id"]: extr1,
                  extr2["id"]: extr2,
                  extr3["id"]: extr3,
                  extr4["id"]: extr4
                  },
                    
          "prio":[
                  {
                   "url":"http://search.51job.com/jobsearch/advance_search.php?lang=c&stype=2",
                   "schd_time": 86400, #根据linkdb里面的抓取时间戳判断,
                   "refere": [],
                   "prio": 0,
                   "weight": 500,
                   "spider_peer": "192.168.2.202"
                   }
                  ]
         }

############################################################################################
#        if headers.has_key('set-cookie'):
#            for cookie in headers['set-cookie']:
#                cookparts = cookie.split(';')
#                cook = cookparts[0]
#                cook.lstrip()
#                k, v = cook.split('=', 1)
#                self.cookies[k.lstrip()] = v.lstrip()
############################################################################################

linkdb_fenzhi_mj = { 
           "id"    : str(ObjectId()), 
           "alias"  : "分智公司地址",
           "encoding":"utf-8",
           "duplicate": "url", #去重策略， url,size
           "spider_peer": "192.168.2.202"
           }

linkdb_fenzhi_mj_list = { 
           "id"    : str(ObjectId()), 
           "alias"  : "面经列表地址",
           "encoding":"utf-8",
           "duplicate": "url", #去重策略， url,size
           }


linkdb_fenzhi_mj_info = { 
           "id"    : str(ObjectId()), 
           "alias"  : "面经详细地址",
           "encoding":"utf-8",
           "duplicate": "url", #去重策略， url,size
           }


#extrs
extr_mj_more = {
         "id"    : str(ObjectId()), 
         "alias"    : "提取面经更多",
         "extr_type"   : "link",  #content, link
         "data"   : {
                     "link"     :'//*[@id=\"FooterPageNav\"]/ul/li/a/@href',
                     },
         "store_linkdb"  : linkdb_fenzhi_mj_list["id"]
         }

extr_mj_list = {
         "id"    : str(ObjectId()), 
         "alias"    : "面经列表提取，主要看有没有下一页地址",
         "extr_type"   : "link",  #content, link
         "data"   : {
                     "link"     :'//*[@id=\"FooterPageNav\"]/ul//a[contains(text(),\"下一页 →\")]/@href',
                     },
         "store_linkdb"  : linkdb_fenzhi_mj_list["id"]
         }

extr_mj_info_url = {
         "id"    : str(ObjectId()), 
         "alias"    : "面经详情地址提取",
         "extr_type"   : "link",  #content, link
         "data"   : {
                     "link"     :'//div[@class=\"interviewReview\"]//h2/a/@href',
                     },
         "store_linkdb"  : linkdb_fenzhi_mj_info["id"]
         }

extr_mj_info = {
         "id"    : str(ObjectId()), 
         "alias"    : "面经内容提取",
         "extr_type"   : "item",  #content, link
         "data"   : {
                     "link"     :'/html/head/title',
                     "link2"     :'/html/head/title',
                     "link3"     :'/html/head/title',
                     "link4"     :'/html/head/title',
                     "link5"     :'/html/head/title',
                     "link6"     :'/html/head/title',
                     "link7"     :'/html/head/title',
                     "link8"     :'/html/head/title',
                     "link9"     :'/html/head/title',
                     "link10"     :'/html/head/title',
                     "link11"     :'/html/head/title',
                     },
         "store_data"  : "fenzhi:gsm"
         }



task_fenzhi_mj = {
          "_id"    : ObjectId(), 
          "alias"    :  "分智网_面经抓取",
          "siteurl"  : "http://www.fenzhi.com",
          "navi"    : {
                         linkdb_fenzhi_mj["id"]   :   {"extrs":[
                                                             extr_mj_more["id"], 
                                                             extr_mj_info_url["id"]
                                                             ],
                                                "enable": 1, # 禁用， 启用
                                                "prio": 1,
                                                "weight": 500
                                                },
                         linkdb_fenzhi_mj_list["id"] : {"extrs":[
                                                                 extr_mj_list["id"], 
                                                                 extr_mj_info_url["id"]
                                                                 ],
                                                "enable": 1, # 禁用， 启用
                                                "prio": 1,
                                                "weight": 500
                                                },

                         linkdb_fenzhi_mj_info["id"] : {"extrs":[
                                                                 extr_mj_info["id"], 
                                                                 ],
                                                "enable": 1, # 禁用， 启用
                                                "prio": 1,
                                                "weight": 500
                                                },
                       },
          "cookie":{
                    "uid":"5000890",
                    "username":"chenwangang1111@163.com",
                    "password":"111111",
                    "PHPSESSID":"soj10hfpet4pujultork7mifh4"
                    },
          "start_links"  :[
                           {
                            "url_id": str(ObjectId()),
                            "linkdb_id":linkdb_fenzhi_mj["id"],
                            "url_type": "mongodb",  #static, increase, mongodb
                            "url":"192.168.2.202:27017:fenzhi:com:siteurl",
                            "replace": [
                                        ["gso", "gsm"]
                                        ],
                            "refere":"",
                            "encoding": "utf-8",
                            "status":"ready",
                           }
                           ],
          "run_times":  0,
          "delay" : 0.5,
          "linkdbs":{
                         linkdb_fenzhi_mj["id"]   : linkdb_fenzhi_mj,
                         linkdb_fenzhi_mj_list["id"] : linkdb_fenzhi_mj_list,

                         linkdb_fenzhi_mj_info["id"] : linkdb_fenzhi_mj_info,
                       },
         
         "extrs":{
                  extr_mj_list["id"]: extr_mj_list,
                  extr_mj_info_url["id"]: extr_mj_info_url,
                  extr_mj_info["id"]: extr_mj_info,
                  extr_mj_more["id"]: extr_mj_more,
                  },
                    
          "prio":[
                  ]
         }
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++分智评论抓取

linkdb_fenzhi_pl = { 
           "id"    : str(ObjectId()), 
           "alias"  : "分智公司地址",
           "encoding":"utf-8",
           "duplicate": "url", #去重策略， url,size
           "spider_peer": "192.168.2.202"
           }

linkdb_fenzhi_pl_list = { 
           "id"    : str(ObjectId()), 
           "alias"  : "评论列表地址",
           "encoding":"utf-8",
           "duplicate": "url", #去重策略， url,size
           }


linkdb_fenzhi_pl_info = { 
           "id"    : str(ObjectId()), 
           "alias"  : "评论详细地址",
           "encoding":"utf-8",
           "duplicate": "url", #去重策略， url,size
           }


#extrs
extr_pl_list = {
         "id"    : str(ObjectId()), 
         "alias"    : "评论列表提取，主要看有没有下一页地址",
         "extr_type"   : "link",  #content, link
         "data"   : {
                     "link"     :'/html/head/title',
                     },
         "store_linkdb"  : linkdb_fenzhi_mj_list["id"]
         }

extr_pl_info_url = {
         "id"    : str(ObjectId()), 
         "alias"    : "评论详情地址提取",
         "extr_type"   : "link",  #content, link
         "data"   : {
                     "link"     :'/html/head/title',
                     },
         "store_linkdb"  : linkdb_fenzhi_mj_info["id"]
         }

extr_pl_info = {
         "id"    : str(ObjectId()), 
         "alias"    : "评论内容提取",
         "siteurl"  : "http://www.fenzhi.com",
         "extr_type"   : "item",  #content, link
         "data"   : {
                     "link"     :'/html/head/title',
                     "link2"     :'/html/head/title',
                     "link3"     :'/html/head/title',
                     "link4"     :'/html/head/title',
                     "link5"     :'/html/head/title',
                     "link6"     :'/html/head/title',
                     "link7"     :'/html/head/title',
                     "link8"     :'/html/head/title',
                     "link9"     :'/html/head/title',
                     "link10"     :'/html/head/title',
                     "link11"     :'/html/head/title',
                     },
         "store_data"  : "fenzhi:gsr"
         }


task_fenzhi_pl = {
          "_id"    : ObjectId(), 
          "alias"    :  "分智网_公司评论抓取",
          "navi"    : {
                         linkdb_fenzhi_pl["id"]   :   {"extrs":[
                                                             extr_pl_list["id"], 
                                                             extr_pl_info_url["id"]
                                                             ],
                                                "enable": 1, # 禁用， 启用
                                                "prio": 1,
                                                "weight": 500
                                                },
                         linkdb_fenzhi_pl_list["id"] : {"extrs":[
                                                                 extr_pl_list["id"], 
                                                                 extr_pl_info_url["id"]
                                                                 ],
                                                "enable": 1, # 禁用， 启用
                                                "prio": 1,
                                                "weight": 500
                                                },

                         linkdb_fenzhi_pl_info["id"] : {"extrs":[
                                                                 extr_pl_info["id"], 
                                                                 ],
                                                "enable": 1, # 禁用， 启用
                                                "prio": 1,
                                                "weight": 500
                                                },
                       },
          "cookie":{
                    "uid":"5000890",
                    "username":"chenwangang1111@163.com",
                    "password":"111111",
                    "PHPSESSID":"soj10hfpet4pujultork7mifh4"
                    },
          "start_links"  :[
                           {
                            "url_id": str(ObjectId()),
                            "linkdb_id":linkdb_fenzhi_pl["id"],
                            "url_type": "mongodb",  #static, increase, mongodb
                            "url":"192.168.2.202:27017:fenzhi:com:siteurl",
                            "replace": [
                                        ["gso", "gsr"]
                                        ],
                            "refere":"",
                            "encoding": "utf-8",
                            "status":"ready",
                           }
                           ],
          "run_times":  0,
          "delay" : 0.5,
          "linkdbs":{
                         linkdb_fenzhi_pl["id"]   : linkdb_fenzhi_pl,
                         linkdb_fenzhi_pl_list["id"] : linkdb_fenzhi_pl_list,

                         linkdb_fenzhi_pl_info["id"] : linkdb_fenzhi_pl_info,
                       },
         
         "extrs":{
                  extr_pl_list["id"]: extr_pl_list,
                  extr_pl_info_url["id"]: extr_pl_info_url,
                  extr_pl_info["id"]: extr_pl_info,
                  },
                    
          "prio":[
                  ]
         }



#************************************用id抓取面经验

linkdb_fenzhi_id_mj = { 
           "id"    : str(ObjectId()), 
           "alias"  : "分智mj地址",
           "encoding":"utf-8",
           "duplicate": "url", #去重策略， url,size
           }


#extrs
extr_id_mj_info = {
         "id"    : str(ObjectId()), 
         "alias"    : "mj内容提取",
         "siteurl"  : "http://www.fenzhi.com",
         "extr_type"   : "item",  #content, link
         "data"   : {
                     "link"     :'/html/head/title',
                     "link2"     :'/html/head/title',
                     "link3"     :'/html/head/title',
                     "link4"     :'/html/head/title',
                     "link5"     :'/html/head/title',
                     "link6"     :'/html/head/title',
                     "link7"     :'/html/head/title',
                     "link8"     :'/html/head/title',
                     "link9"     :'/html/head/title',
                     "link10"     :'/html/head/title',
                     "link11"     :'/html/head/title',
                     },
         "store_data"  : "fenzhi:gsm_id"
         }


task_fenzhi_mj_id = {
          "_id"    : ObjectId(), 
          "alias"    :  "分智网_面经——id抓取",
          "navi"    : {
                         linkdb_fenzhi_id_mj["id"]   :   {"extrs":[
                                                             extr_id_mj_info["id"]
                                                             ],
                                                "enable": 1, # 禁用， 启用
                                                "prio": 1,
                                                "weight": 500
                                                },
                       },
          "cookie":{
                    "uid":"5000890",
                    "username":"chenwangang1111@163.com",
                    "password":"111111",
                    "PHPSESSID":"soj10hfpet4pujultork7mifh4"
                    },
          "start_links"  :[
                           {
                            "url_id": str(ObjectId()),
                            "linkdb_id":linkdb_fenzhi_id_mj["id"],
                            "url_type": "increase",  #static, increase, mongodb
                            "url":"http://www.fenzhi.com/gsmsh{1, 270000, 1}.html",
                            "replace": [],
                            "refere":"",
                            "encoding": "utf-8",
                            "status":"ready",
                           }
                           ],
          "run_times":  0,
          "delay" : 0.5,
          "linkdbs":{
                         linkdb_fenzhi_id_mj["id"]   : linkdb_fenzhi_id_mj,
                       },
         
         "extrs":{
                  extr_id_mj_info["id"]: extr_id_mj_info,
                  },
                    
          "prio":[
                  ]
         }


#************************************用id抓取分智评论

linkdb_fenzhi_id_pl = { 
           "id"    : str(ObjectId()), 
           "alias"  : "分智pl地址",
           "encoding":"utf-8",
           "duplicate": "url", #去重策略， url,size
           }


#extrs
extr_id_pl_info = {
         "id"    : str(ObjectId()), 
         "alias"    : "pl内容提取",
         "siteurl"  : "http://www.fenzhi.com",
         "extr_type"   : "item",  #content, link
         "data"   : {
                     "link"     :'/html/head/title',
                     "link2"     :'/html/head/title',
                     "link3"     :'/html/head/title',
                     "link4"     :'/html/head/title',
                     "link5"     :'/html/head/title',
                     "link6"     :'/html/head/title',
                     "link7"     :'/html/head/title',
                     "link8"     :'/html/head/title',
                     "link9"     :'/html/head/title',
                     "link10"     :'/html/head/title',
                     "link11"     :'/html/head/title',
                     },
         "store_data"  : "fenzhi:gsr_id"
         }


task_fenzhi_pl_id = {
          "_id"    : ObjectId(), 
          "alias"    :  "分智网_评论——id抓取",
          "navi"    : {
                         linkdb_fenzhi_id_mj["id"]   :   {"extrs":[
                                                             extr_id_pl_info["id"]
                                                             ],
                                                "enable": 1, # 禁用， 启用
                                                "prio": 1,
                                                "weight": 500
                                                },
                       },
          "cookie":{
                    "uid":"5000890",
                    "username":"chenwangang1111@163.com",
                    "password":"111111",
                    "PHPSESSID":"soj10hfpet4pujultork7mifh4"
                    },
          "start_links"  :[
                           {
                            "url_id": str(ObjectId()),
                            "linkdb_id":linkdb_fenzhi_id_pl["id"],
                            "url_type": "increase",  #static, increase, mongodb
                            "url":"http://www.fenzhi.com/pl{1, 150000, 1}.html",
                            "replace": [],
                            "refere":"",
                            "encoding": "utf-8",
                            "status":"ready",
                           }
                           ],
          "run_times":  0,
          "delay" : 0.5,
          "linkdbs":{
                         linkdb_fenzhi_id_pl["id"]   : linkdb_fenzhi_id_pl,
                       },
         
         "extrs":{
                  extr_id_pl_info["id"]: extr_id_pl_info,
                  },
                    
          "prio":[
                  ]
         }


######################################################################登录得到cookie
extr_login = {
             "id"    : str(ObjectId()), 
             "alias"    : "登录分智页面",
             "type"     : "action",  #content, link
             "actions"  : [
                           {
                            "act":"input",
                            "dom": '//table[@class="navBold"]//tr[1]/td[3]/a/@href',
                            "text": "$username"
                            },
                           {
                            "act":"input",
                            "dom": '//table[@class="navBold"]//tr[1]/td[3]/a/@href',
                            "text": "$password"
                            },
                           {
                            "act":"click",
                            "dom": '//table[@class="navBold"]//tr[1]/td[3]/a/@href',
                            },
                           ],
             "data"   : {
                         "cookie"     :'$cookie',
                         },
             
             "store"  : "51jobs"
             }


linkdb_login_fenzhi = { 
                       "id"    : str(ObjectId()), 
                       "alias"  : "分智登录页",
                       "encoding":"gbk",
                       "prio"   : 0,
                       "weight" : 500,
                       }


task_login_fenzhi = {
          "_id"     : ObjectId(), 
          "alias"   :  "登陆分智网得到cookie",
          "navi"    : {
                           linkdb_login_fenzhi["id"]   :   [extr_login["id"]],
                       },
          "cookie" : "",
          "start_links"  :[
                           {
                           "url":"http://www.fenzhi.com/user.php?r=/",
                          "refere":"",
                          "status":"ready",
                          "hit_count": 0,
                          "name":linkdb_login_fenzhi
                           }
                           ],
          "run_times":  0,
          "delay" : 0,
          
          "accounts" : accounts_fenzhi,
          "linkdbs":[
                     linkdb_login_fenzhi,
                     ],
         "extrs":[
                  extr_login
                  ],
          "prio":[
                  {
                   "url":"http://search.51job.com/jobsearch/advance_search.php?lang=c&stype=2",
                   "schd_time": 86400, #根据linkdb里面的抓取时间戳判断,
                   "refere": [],
                   "prio": 0,
                   "weight": 500,
                   "spider_peer": "192.168.2.202"
                   }
                  ]
         }


#####################################################################注册分智网

extr_regist = {
             "id"    : str(ObjectId()), 
             "alias"    : "登录分智页面",
             "type"     : "action",  #content, link
             "actions"  : [
                           {
                            "act":"input",
                            "dom": '//table[@class="navBold"]//tr[1]/td[3]/a/@href',
                            "text": "$username"
                            },
                           {
                            "act":"input",
                            "dom": '//table[@class="navBold"]//tr[1]/td[3]/a/@href',
                            "text": "$password"
                            },
                           {
                            "act":"click",
                            "dom": '//table[@class="navBold"]//tr[1]/td[3]/a/@href',
                            },
                           ],
             "data"   : {
                         "cookie"     :'$cookie',
                         },
             
             "store"  : "51jobs"
             }


linkdb_login_fenzhi2 = { 
                       "id"    : str(ObjectId()), 
                       "alias"  : "分智登录页",
                       "encoding":"gbk",
                       "prio"   : 0,
                       "weight" : 500,
                       }


task_regist_fenzhi = {
                      "_id"     : ObjectId(), 
                      "alias"   :  "登陆分智网得到cookie",
                      "navi"    : {
                                       linkdb_login_fenzhi2["id"]   :   [extr_regist["id"]],
                                   },
                      "cookie" : "",
                      "start_links"  :[
                                       {
                                       "url":"http://www.fenzhi.com/user.php?r=/",
                                      "refere":"",
                                      "status":"ready",
                                      "hit_count": 0,
                                      "name":linkdb_login_fenzhi2
                                       }
                                       ],
                      "run_times":  0,
                      "delay" : 0,
                      
                      "accounts" : accounts_fenzhi,
                      "linkdbs":[
                                 linkdb_login_fenzhi2,
                                 ],
                     "extrs":[
                              extr_login
                              ],
                      "prio":[
                              {
                               "url":"http://search.51job.com/jobsearch/advance_search.php?lang=c&stype=2",
                               "schd_time": 86400, #根据linkdb里面的抓取时间戳判断,
                               "refere": [],
                               "prio": 0,
                               "weight": 500,
                               "spider_peer": "192.168.2.202"
                               }
                              ]
                     }






import time

import pymongo


def initData(host, port):

    conn = pymongo.Connection(host, port)
    conn.drop_database("sys")
    db = conn["sys"]
    
#    for link in task1["start_links"]:
#        db["linkdb"].insert(link)


    print "task1:", db["task"].insert(task1)
    print "task_fenzhi_mj_id: ", db["task"].insert(task_fenzhi_mj_id)
    print "task_fenzhi_pl_id: ", db["task"].insert(task_fenzhi_pl_id)
    print "task_fenzhi_mj: ", db["task"].insert(task_fenzhi_mj)
    print "task_fenzhi_pl: ", db["task"].insert(task_fenzhi_pl)
    
    
    
if __name__ == "__main__":
    initData("localhost", 27017)




