#coding: utf-8
###############################################################################
##
##  Copyright 2011 Tavendo GmbH
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##      http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##
###############################################################################

import sys
import os
sys.path.insert(1, os.path.abspath("../"))


from twisted.python import log
from twisted.internet import reactor
from twisted.internet.defer import Deferred, \
                                   DeferredList, \
                                   gatherResults, \
                                   returnValue, \
                                   inlineCallbacks
from autobahn.websocket import connectWS
from autobahn.wamp import WampClientFactory, WampClientProtocol
import redis
import pymongo
from bson.objectid import ObjectId
from twisted.web.client import getPage, HTTPClientFactory, _parse
#from ExTx.client import getPage

from lxml import etree
from scrapy.selector import HtmlXPathSelector
from scrapy.http import Response, HtmlResponse
from scrapy.http.response.text import TextResponse

import urllib2, gzip, StringIO



import time
from autobahn.util import newid

print newid()

def xpathExtract(body, encoding, url, extr={}):

    try:
        body = gzip.GzipFile(fileobj=StringIO.StringIO(body)).read()
    except:
        pass

#    print body
    _res = TextResponse(url=url, body=body, encoding=encoding)
    
    hxs = HtmlXPathSelector(_res)
    
    data = {}
    for k, v in extr.iteritems():
        dl = []
        for d in hxs.select(v):
            dl.append(d.extract())
        if len(dl) == 1:
            dl = dl[0]
        data[k] = dl
        
    data["siteurl"] = url
    data["timestamp"] = time.time()
    
    
    return data



def getpage_success(result, **args):
    
    rets = xpathExtract(result, args["encoding"], args["url"], args["data"])
    
    for k, v in rets.iteritems():
        try:
            print "%s: %s" % (k, v.encode("utf-8"))
        except:
            print k, v
        
    reactor.stop()


def getpage_err(result, **args):
    pass

import os
import urllib

if __name__ == '__main__':

   log.startLogging(sys.stdout)
#   os.environ['http_proxy'] = "http://192.168.2.202:3128"

#   print urllib.urlopen("http://iframe.ip138.com/ic.asp").read()

   args = {
           "encoding": "utf-8",
#           "url" : "http://iframe.ip138.com/ic.asp",
           "url" : "http://www.fenzhi.com/gsmsh5330.html",
           "data" : {
                      "a1":'//*[@id="empReview_5960"]/div[3]/div[1]',
                      "a2" : '//*[@class="oneQuestion"]/text()',
                     },
           "cookie":{
                    #"uid":"5000890",
                    #"username":"chenwangang1111@163.com",
                    #"password":"111111",
                    "PHPSESSID":"6bsn39mc9pbafj17og4udbl0c6"
                    },
           }

   proxy = {
            "host":"192.168.2.202",
            "port":3128
            }

   getPage(args["url"],  cookies=args["cookie"])\
   .addCallbacks(
              getpage_success,
              getpage_err,
              callbackKeywords=args,
              errbackKeywords=args,
              )
           
   reactor.run()
