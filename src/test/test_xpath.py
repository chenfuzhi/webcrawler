#coding:utf-8

from lxml import etree
import urllib
from scrapy.selector import HtmlXPathSelector
from scrapy.http import Response, HtmlResponse
from scrapy.http.response.text import TextResponse

import urllib2, gzip, StringIO

conf = {
        "encoding": "gb2312",
        "gizp": True,
        "url"   : "http://search.51job.com/list/160000%252C00,000000,0000,00,3,99,%2B,2,2.html?lang=c&stype=2&postchannel=0000&workyear=99&cotype=99&degreefrom=99&jobterm=01&lonlat=0%2C0&radius=-1&ord_field=0&list_type=0&confirmdate=9",
        "xpath" : '//table[@id="resultList"]/tr[@class="tr0"]//a[@class="jobname"]/@href'
        }




def extract(body, encoding, url, extr={}):
    
    
    try:
        body = gzip.GzipFile(fileobj=StringIO.StringIO(body)).read()
    except:
        pass
    
    _res = TextResponse(url=url, body=body, encoding=encoding)
    
    hxs = HtmlXPathSelector(_res)
    
    data = {}
    for k, v in extr.iteritems():
        dl = []
        for d in hxs.select(v):
            dl.append(d.extract())
        data[k] = dl
    return data




if __name__ == "__main__":
    body = urllib.urlopen(conf["url"]).read()
    rets = extract(body, conf["encoding"], conf["url"], extr={"link":conf["xpath"]})
    print rets
    
    
#print hxs.select(conf["xpath"])[0].extract()





