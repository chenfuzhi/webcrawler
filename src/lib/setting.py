import pymongo

#-*- coding:utf-8 -*-
BOT_NAME = 'dcrawler'
BOT_VERSION = '1.0'

SPIDER_MODULES = ['project.spiders']
NEWSPIDER_MODULE = 'project.spiders'
USER_AGENT = 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)'

CONCURRENT_REQUESTS = 3
REDIRECT_ENABLED = False

# redis linkdb
REDIS_HOST = '192.168.2.201'
REDIS_PORT = 6379

# mongodb config
SYSMONGO_HOST = 'localhost'
SYSMONGO_PORT = 27017
SYSMONGO_DB = 'sys'

# item store
DATAMONGO_HOST = 'localhost'
DATAMONGO_PORT = 27017
DATAMONGO_DB = 'store'






