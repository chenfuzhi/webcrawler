#coding: utf-8
from bson.objectid import ObjectId
import txmongo
from twisted.internet import defer, reactor
import setting
import json

from txmongo._pymongo.objectid import ObjectId as txObjectId


class mongoDB:
    def __init__(self):
        self.conn = None
        
    @defer.inlineCallbacks
    def connect(self, setting):
        '''
            # item store mongodb host
            STORE_HOST = "192.168.2.202"
            STORE_PORT = 27017
            
            # link store mongodb host
            LINK_HOST = "192.168.2.202"
            LINK_PORT = 27017
        '''
        
        self.conn = yield txmongo.MongoConnectionPool(host=setting.SYSMONGO_HOST, port=setting.SYSMONGO_PORT, pool_size=5)
        self.conn_store = yield txmongo.MongoConnectionPool(host=setting.STORE_HOST, port=setting.STORE_PORT, pool_size=5)
        self.conn_link = yield txmongo.MongoConnectionPool(host=setting.LINK_HOST, port=setting.LINK_PORT, pool_size=5)
        
        self.sys = self.conn[setting.SYSMONGO_DB]
        print self.conn
    
    def getTaskConf(self, task_id):
        return self.sys["task"].find_one({"_id":txObjectId(task_id)})

    def store_finish_url(self, url_info):
        self.conn_link["linkdb"]["task"].save(url_info)
        
        
    @defer.inlineCallbacks
    def store_item_data(self, item, store_uri):
        '''
        
        
        '''
        db, c = store_uri.split(":")
        coll = self.conn_store[db][c]
        yield coll.save(item)

@defer.inlineCallbacks
def start():
    r = mongoDB()
    yield r.connect(setting)
    
    print "ddddd"


if __name__ == "__main__":
    start()
    reactor.run()



