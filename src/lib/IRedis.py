#coding:utf-8


import setting

import txmongo
import txredisapi as redis
from twisted.internet import defer, reactor
import json
import time
from bson.objectid import ObjectId
import re
import copy
import pymongo

#yield redis.ConnectionPool(host="localhost", port=6379, poolsize=10)
#conn_mongo  = yield txmongo.MongoConnectionPool(host="127.0.0.1", port=27017, pool_size=5)

class redisDB:
    
    def __init__(self):
        self.redis = None
        
    @defer.inlineCallbacks
    def connect(self, setting):
        self.redis = yield redis.ConnectionPool(host=setting.REDIS_HOST, port=setting.REDIS_PORT, poolsize=10)
    
    @defer.inlineCallbacks
    def resetLinkdbStatus(self, url_id):
        '''
        :param url_id:
        '''
        yield self.redis.hdel(self.key_running, url_id)
        yield self.redis.sadd(self.key_ready, url_id)
    
    
    @defer.inlineCallbacks
    def initTask(self, task):
        MAX_INPUT_URLS = 100000
        self.task = task.conf
        
        cnt = 0
        
        for url in task.conf["start_links"]:
            if not url.has_key("url_type"):
                print "没有设置url_type"
                reactor.stop()
            
            if url["url_type"] == "increase":
                
                re_range = re.compile(u"\{(\d+), (\d+), (\d+)\}")
                re_replace = re.compile(u"\{.*\}")
                _start, _end, _step = map(int, re_range.findall(url["url"])[0])
                
                print "url", url["url"]
                print "extr_range", _start, _end, _step, re_range.findall(url["url"])
                for x in xrange(_start, _end, _step):
                    _url = copy.copy(url)
                    gen_url = re_replace.sub(str(x), _url["url"])
                    _url["url"] = gen_url
                    _url["url_id"] = str(ObjectId())
                    yield self.redis.hset(self.key_linkdb, _url["url_id"], json.dumps(_url))
                    yield self.redis.sadd(self.key_ready, _url["url_id"])
                    print "add_url to redis:", gen_url
                
            elif url["url_type"] == "static":
                ret = yield self.redis.hset(self.key_linkdb, url["url_id"], json.dumps(url))
                ret = yield self.redis.sadd(self.key_ready, url["url_id"])
            
            elif url["url_type"] == "mongodb":
                _h, _p, _d, _c, _f = url["url"].split(":")
                conn = pymongo.Connection(_h, int(_p))
                condition = {}
                if url.has_key("condition"):
                    condition = url["condition"]
                skip = 0
                limit = 2000
                while True:
                    rows = conn[_d][_c].find(condition, skip=skip, limit=limit)
                    if rows.count(with_limit_and_skip=True) == 0:
                        break
                    for r in rows:
                        _url = copy.copy(url)
                        _url["url"] = r[_f]
                        if _url.has_key("replace"):
                            for rp in _url["replace"]:
                                _url["url"] = _url["url"].replace(rp[0], rp[1])
                                
                        _url["url_id"] = str(ObjectId())
                        yield self.redis.hset(self.key_linkdb, _url["url_id"], json.dumps(_url))
                        yield self.redis.sadd(self.key_ready, _url["url_id"])
                        print "add_url to redis:", _url["url"]
                    
                    skip += limit
        
        defer.returnValue(None)
            
    
    @defer.inlineCallbacks
    def getReadyLink(self, spider_id):
        '''
        得到下一个要抓取的地址
        
        返回link结构：
        '''
        url_id = yield self.redis.spop(self.key_ready)
        if url_id:
            yield self.redis.hset(self.key_running, url_id, spider_id)
            ret = yield self.redis.hget(self.key_linkdb, url_id)
            defer.returnValue(ret)
        
    @defer.inlineCallbacks   
    def store_link(self, item_lk):
        ret = yield self.redis.hset(self.key_linkdb, item_lk["url_id"], json.dumps(item_lk))
        ret = yield self.redis.sadd(self.key_ready, item_lk["url_id"])

    
    @defer.inlineCallbacks   
    def delete_finish_url(self, url_id, callback):
        '''
        :param url_id:
        running->finish
        '''
        url_info = yield self.redis.hget(self.key_linkdb, url_id)
        yield self.redis.hdel(self.key_linkdb, url_id)
        yield self.redis.hdel(self.key_running, url_id)
        if url_info:
            callback(json.loads(url_info))
        #defer.returnValue(json.loads(url_info))



def increase_url():
    pass

@defer.inlineCallbacks
def start():
    r = redisDB(setting)
    yield r.setup()
    
    print "ddddd"


if __name__ == "__main__":
    start()
    reactor.run()





