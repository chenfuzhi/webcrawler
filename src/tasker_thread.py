# -*- coding: utf-8 -*-
###############################################################################
##
##  Copyright 2011 Tavendo GmbH
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##      http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##
###############################################################################

import sys
from twisted.python import log
from twisted.internet import reactor
from twisted.internet.defer import Deferred, \
                                   DeferredList, \
                                   gatherResults, \
                                   returnValue, \
                                   inlineCallbacks
from autobahn.websocket import connectWS
from autobahn.wamp import WampClientFactory, WampClientProtocol
import redis
import pymongo
from bson.objectid import ObjectId
import json
from Queue import Queue
import time


import setting
from IMongodb import systemMongodb


class TaskClientProtocol(WampClientProtocol):
    """
    Demonstrates simple Publish & Subscribe (PubSub) with Autobahn WebSockets.
    """

    def connectionLost(self, reason):
        WampClientProtocol.connectionLost(self, reason)
        self.connected = 0
        
        #重置缓存队列： ready
        for k, v in self.cache.iteritems():
            self.factory.mongo.db["linkdb"].update({"_id":ObjectId(v["url_id"])}, {"$set":{"status":"ready"}})
        self.cache = {}
        
    def start_clienter(self, topicUri, event):
        print topicUri, event
        print "start_process"
        
        
    def refreshSpiders(self, result):
        
        
        self.spiders = result
        
        #在缓存队列里面的无对应蜘蛛ID的重置为： ready
        popks = []
        for k, v in self.cache.iteritems():
            if v["spider"] not in self.spiders:
                self.factory.mongo.db["linkdb"].update({"_id":ObjectId(v["url_id"])}, {"$set":{"status":"ready"}})
                popks.append(k)
        for k in popks:
            self.cache.pop(k)
            
        log.msg("刷新蜘蛛列表：")
        print self.spiders
        
    def getOneSpider(self):
        self.sel_spider += 1
        index = self.sel_spider % len(self.spiders)
        return self.spiders[index]["sid"]
        
        
    def onSessionOpen(self):
        
        self.subscribe("tasker", self.process_msg)
        log.msg("订阅tasker完成。。")
        
        
        self.spiders = {}
        self.conf = self.factory.mongo.getTaskConf({"alias"    :  "51job"})
        self.sel_spider = 0
        self.cache = {} # 缓存
        self.errs = {}
        self.publish_finish = False
        
        self.call("clients", "spider").addCallback(self.refreshSpiders)
        
        log.msg("等待3秒获取蜘蛛列表。。")
        
        reactor.callLater(3, self.running)
        
    def running(self):
        if not self.connected: 
            log.msg("已经失去连接， 等待重新连接")
            return
        
        if len(self.spiders) == 0:
            reactor.callLater(3, self.running)
            log.msg("没有可用的蜘蛛，等待3秒")
        else:
            if len(self.cache) < 10000:
                crawl_cmd = self.make_next_crawl()
                
                if crawl_cmd:
                    msg = {
                           "type" : "crawl",
                           "data" : crawl_cmd
                           }
                    self.publish("spider", msg, eligible=[crawl_cmd["spider"]])
                    reactor.callLater(0.01, self.running)
                else:
                    reactor.callLater(3, self.running)
                    log.msg("暂时没有可抓取的链接了, 等待3秒")
            else:
                log.msg("已经达到最大的发送队列, 等待3秒")
                reactor.callLater(3, self.running)
                    
                
    def make_next_crawl(self):
        link = self.factory.mongo.getNextLink(self.conf)
        if link:
            link["_id"] = str(link["_id"])
            crawl_cmd = {}
            crawl_cmd.setdefault("pid", str(ObjectId()))
            crawl_cmd.setdefault("url_id", link["_id"])
            crawl_cmd.setdefault("url", link["url"])
            crawl_cmd.setdefault("encoding", link["name"]["encoding"])
            crawl_cmd.setdefault("spider", self.getOneSpider())
            crawl_cmd.setdefault("tasker", self.session_id)
            crawl_cmd.setdefault("time", int(time.time()))
            
            crawl_cmd.setdefault("extrs", self.getExtrs(link["name"]["id"]))
            
            log.msg("生成一个抓取指令:%s,地址：%s" % (crawl_cmd["pid"], link["url"].encode("utf-8")))
            self.cache.setdefault(crawl_cmd["pid"], crawl_cmd)
            return crawl_cmd
        return None
    
    def getExtrs(self, link_id):
        rets = []
        for e_id in self.conf["navi"][link_id]:
            for extr in self.conf["extrs"]:
                if extr["id"] == e_id:
                    rets.append(extr)
        return rets
            
    def getLinkdb(self, link_id):
        for ldb in self.conf["linkdbs"]:
            if ldb["id"] == link_id:
                return ldb
        
    def process_msg(self, topicUri, event):
        log.msg("收到消息：%s" % str(event))
        
        if event["type"] == "response":
            if self.cache.has_key(event["pid"]):
                self.cache.pop(event["pid"])
                
            if event["success"] == 1:
                self.factory.mongo.db["linkdb"].update({"_id":ObjectId(event["url_id"])}, {"$set":{"status":"finish"}})
                
                for extr in event["extrs"]:
                    if extr["type"] == "item":
                        self.factory.mongo.store_data(extr["data"], extr["store"])
                        
                    elif extr["type"] == "link":
                        for l in extr["data"]["link"]:
                            
                            item_lk = {
                                    "name": self.getLinkdb(extr["linkdb"]),
                                    "url"  : l,
                                    "refere"  : event["url"],
                                    "time"  :  int(time.time()),
                                    "status": "ready"
                                    }
                            self.factory.mongo.store_link(item_lk)
            else:
                self.factory.mongo.db["linkdb"].update({"_id":ObjectId(event["url_id"])}, {"$set":{"status":"ready", "err":event}})        
                    
                    
        elif event["type"] == "broadcast":
            self.call("clients", "spider").addCallback(self.refreshSpiders)
        



class TaskClientFactory(WampClientFactory):
    def __init__(self, url, debug = False, debugCodePaths = False, debugWamp = False, debugApp = False):
        WampClientFactory.__init__(self, url, debug = debug, debugCodePaths = debugCodePaths)
        self.mongo = systemMongodb(setting)
        print "self.mongo connection"
    

    def startFactory(self):
        print "startFactory"
        pass

        
        
    def clientConnectionFailed(self, connector, reason):
        log.msg("连接失败, 3秒后重试")
        reactor.callLater(3, connector.connect)
        
        
    def clientConnectionLost(self, connector, reason):
        log.msg("连接已经失去, 3秒后重试")
        reactor.callLater(3, connector.connect)

if __name__ == '__main__':
   log.startLogging(sys.stdout)
   factory = TaskClientFactory("ws://localhost:9000")
   factory.protocol = TaskClientProtocol
   connectWS(factory)
   reactor.run()
   
   
   
   
