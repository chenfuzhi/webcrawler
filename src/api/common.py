#coding:utf-8


import os
from mongokit import *

import json
from bson.json_util import default, object_hook

from api import app
from flask import Flask, request


#mongo_orm.blog1
@app.route("/api/tree/", methods=['GET'])
def get_tree():
    '''
    '''
    node = request.args['node']
    ret = []
    if node == "task":
        ret.append({"id":"1", "text":"Contact", "node":"task", "leaf": True})
        ret.append({"id":"2", "text":"Login", "node":"rule", "leaf": True})
        ret.append({"id":"3", "text":"Register", "node":"spider", "leaf": True})
    elif node == "rule":
        ret.append({"id":"4", "text":"51规则", "node":"task"})
        ret.append({"id":"5", "text":"lietou规则", "node":"rule"})
        ret.append({"id":"6", "text":"招聘规则", "node":"spider"})
        
    return json.dumps(ret)

@app.route("/api/list/<table>", methods=['GET'])
def get_list(table):
    '''
    http://localhost:5000/api/list/task?fields=['aaa',%20'bbb',%20'ccc']&find={}&sort=[]&skip=0&limit=10
    :param table:
    '''
    
    _find = request.args['find']
    _fields = request.args['fields']
    _sort = request.args['sort']
    _skip = request.args['skip']
    _limit = request.args['limit']
    
    app.logger.warning("dddddddd")
    rows = db[table].find(eval(_find), fields=eval(_fields), sort=eval(_sort), skip=int(_skip), limit=int(_limit))
    return json.dumps(list(rows), default=default)

@app.route("/api/one/<table>", methods=['GET'])
def get_one(table):
    _find = request.args['find']
    _fields = request.args['fields']
    row = db[table].find_one(eval(_find), fields=eval(_fields))
    return json.dumps(row, default=default)

@app.route("/api/add/<table>", methods=['POST'])
def add(table):
    '''
    http://localhost:5000/api/add/task?author=chenfuzhi&title=good_job
    :param table:
    '''
    doc = {}
    pass_keys = ['__debuger__']
    for k, v in request.args.iteritems():
        if k in pass_keys: continue
        doc[k] = v
    try:
        _id = db[table].save(doc)
        return '{"success":"ok", "_id":"%s"}' % _id
    except Exception, e:
        return '{"error":"%s"}' % e
    

@app.route("/api/update/<table>", methods=['GET', 'POST'])
def update(table):
    _find = request.args['find']
    _set = request.args['set']
    try:
        db[table].update(eval(_find), eval(_set), safe=True, upsert=True, multi=True)
        return '{"success":"ok"}'
    except Exception, e:
        return '{"error":"%s"}' % e
    

@app.route("/api/delete/<table>", methods=['GET'])
def delete(table):
    _find = request.args['find']
    try:
        db[table].remove(eval(_find))
        return '{"success":"ok"}'
    except Exception, e:
        return '{"error":"%s"}' % e

print "common.py loading"
    