#coding:utf-8

import os
from mongokit import *

import json
from bson.json_util import default, object_hook
from bson.objectid import ObjectId
from api import app
from flask import Flask, request
import time
import pymongo
__conn__ = pymongo.Connection("localhost", 27017)
__database__ = __conn__["sys"]


from webcrawler import __RUNNING__, __repeater_port__


    
@app.route("/api/mongodb/add/", methods=['POST', 'GET'])
def mongodb_add():
    '''
    '''
    print request.json
    text = request.json['text']
    collection = request.json['collection']
    _id = __database__[collection].insert({"text":text})
    
    ret = {
                "success": True,
                "data": {"id": str(_id)},
                "msg":'增加节点成功'
           }
    return json.dumps(ret)



#mongo_orm.blog1
@app.route("/api/mongodb/del/", methods=['GET', 'POST'])
def mongodb_del():
    '''
    '''
    _id = request.json['id']
    collection = request.json['collection']
    __database__[collection].remove({"_id":ObjectId(_id)})
    ret = {
                "success": True,
                "msg":"删除成功"
           }
    return json.dumps(ret)

    
    
@app.route("/api/mongodb/update/", methods=['GET', 'POST'])
def mongodb_update():
    '''
    '''
    collection = request.json.pop("collection")
    _id = request.json.pop("id")
    __database__[collection].update({"_id":ObjectId(_id)}, request.json)
    ret = {
                "success": True,
                "msg":"修改成功"
           }
    return json.dumps(ret)

