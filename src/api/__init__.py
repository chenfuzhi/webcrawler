
from flask import Flask
app = Flask(__name__, static_folder='../ui')


import setting
import pymongo

__all__ = ["app", "common", 'mongodb', 'SYS']

__conn__ = pymongo.Connection(setting.SYSMONGO_HOST, setting.SYSMONGO_PORT)
SYS = __conn__[setting.SYSMONGO_DB]


from api import *
from api.task.start_links import *
