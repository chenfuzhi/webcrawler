#coding:utf-8

import os
from mongokit import *

import json
from bson.json_util import default, object_hook
from bson.objectid import ObjectId
from api import app, SYS
from flask import Flask, request
import time
import pymongo



@app.route("/api/task/info/", methods=['GET', 'POST'])
def mongodb_task_info():
    '''
    '''
    _id = request.values.get("id", "")
    _task = SYS["task"]
    row = _task.find_one({"_id" : ObjectId(_id)})
    row["id"] = str(row.pop("_id"))
    msg = {
              "success":1,
              "data": row,
              "msg" :"ok"
           }
    return json.dumps(msg, indent=2)

