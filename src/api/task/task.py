#coding:utf-8

import os
from mongokit import *

import json
from bson.json_util import default, object_hook
from bson.objectid import ObjectId
from api import app
from flask import Flask, request
import time
import pymongo
from api import SYS


@app.route("/api/task/read/", methods=['GET', 'POST'])
def task_read():
    '''
    '''
    node = request.values.get("node", "")
    if node == "webcrawler":
        msg = {
               "data" : [
                            {
                                "id":'spider',
                                "text": "֩蜘蛛列表",
                            },
                            { 
                               "id":'tasker',
                               "text": "任务列表", 
                            }
                         ],
               "success" : 1,
               'message' : "ok"
               }
        return json.dumps(msg)
    elif node == "tasker":
        _task = SYS["task"]
        
        rows = _task.find({})
        
        data = []
        for r in rows:
            r["id"] = str(r["_id"])
            r.pop("_id")
            r["text"] = r["alias"] 
            r["leaf"] = 1
            r["icon"] = "resources/images/logo.png"
            
            data.append(r)
        msg = {
               "data" : data,
               "success" : 1,
               'message' : "ok"
               }
        return json.dumps(msg)
    
    elif node == "spider":
        
        pass
    
    return {"error":0}

