#coding:utf-8

import os
from mongokit import *

import json
from bson.json_util import default, object_hook
from bson.objectid import ObjectId
from api import app, SYS
from flask import Flask, request
import time
import pymongo


@app.route("/api/task/linkdbs/read/", methods=['GET', 'POST'])
def task_linkdbs_read():
    '''
    '''
    task_id = request.values.get("id", "")
    db_task = SYS["task"]
    row = db_task.find_one({"_id":ObjectId(task_id)},
                           fields=['linkdbs']
                           )
    rets = []
    for k, v in row['linkdbs'].iteritems():
        rets.append(v)
        
    msg = {
              "success":1,
              "data": rets,
              "msg" :"ok"
           }
    return json.dumps(msg, indent=2)

