#coding:utf-8

import os
from mongokit import *

import json
from bson.json_util import default, object_hook
from bson.objectid import ObjectId
from api import app, SYS
from flask import Flask, request
import time
import pymongo


from webcrawler import __RUNNING__, __repeater_port__


@app.route("/api/task/start_links/read/", methods=['GET', 'POST'])
def task_start_links_read():
    '''
    '''
    task_id = request.values.get("task_id", "")
    start = request.values.get("start", 0)
    limit = request.values.get("limit", 1)
    db_task = SYS["task"]
    
    find = {"_id":ObjectId(task_id)}

    rows = db_task.find(find,
                           fields={
                                   'start_links':{"$slice":map(int, [start, limit])}, 
                                   "_id":True
                                   },
                           )
    rets = []
    for row in rows:
        rets.append(row["start_links"])
    msg = {
              "success":1,
              "data": rets,
              "msg" :"ok"
           }
    return json.dumps(msg, indent=2, default=default)

@app.route("/api/task/start_links/create/", methods=['GET', 'POST'])
def task_start_links_create():
    '''
    '''
    
    link = request.json
    link.pop('id')
    task_id = request.values.get("task_id", "")
    db_task = SYS["task"]
    db_task.update({"_id":ObjectId(task_id)},
                   {"$push":{"start_links":link}}
                   )
    msg = {
              "success":True,
              "msg" :"ok"
           }
    return json.dumps(msg, indent=2)

@app.route("/api/task/start_links/update/", methods=['GET', 'POST'])
def task_start_links_update():
    '''
    '''
    task_id = request.values.get("id", "")
    slice = request.values.get("slice", 0)
    num = request.values.get("num", 1)
    db_task = SYS["task"]
    row = db_task.find_one(fields={
                                   'start_links':{"$slice":[slice, num]}, 
                                   "_id" : ObjectId(_id)
                                   },
                           )
    row["id"] = str(row.pop("_id"))
    msg = {
              "success":1,
              "data": row,
              "msg" :"ok"
           }
    return json.dumps(msg, indent=2)

@app.route("/api/task/start_links/destroy/", methods=['GET', 'POST'])
def task_start_links_destroy():
    '''
    '''
    task_id = request.values.get("id", "")
    slice = request.values.get("slice", 0)
    num = request.values.get("num", 1)
    db_task = SYS["task"]
    row = db_task.find_one(fields={
                                   'start_links':{"$slice":[slice, num]}, 
                                   "_id" : ObjectId(_id)
                                   },
                           )
    row["id"] = str(row.pop("_id"))
    msg = {
              "success":1,
              "data": row,
              "msg" :"ok"
           }
    return json.dumps(msg, indent=2)
